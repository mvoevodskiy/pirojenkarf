id: 100
source: 1
name: getParentsIds
properties: 'a:0:{}'

-----

$doc = $modx->getOption('doc',$scriptProperties,'');
$level = $modx->getOption('level',$scriptProperties,'1');
if($doc != ''){

	$parentIds = $modx->getParentIds($doc);
	return $parentIds[$level];

}else{

	return;
}