id: 99
source: 1
name: getMainParentTitle
properties: 'a:0:{}'

-----

$res = $modx->getObject('modResource', $id);
while($res->get('parent') != 0) {
    $res = $res->getOne('Parent');
}
return $res->get('pagetitle');