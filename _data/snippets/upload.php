id: 29
source: 1
name: upload
properties: 'a:0:{}'

-----

exit;

function setTV($id, $name, $val) {
	global $modx;
  	$tv = $modx->getObject('modTemplateVar', array('name' => $name));
 	$tv->setValue($id, $val);
    $tv->save();
}

function copyImage($id, $image) {
	$path = 'assets/img/products/' . $id . '.jpg';
	copy($image, $path);
	return $path;
}

function uploadCat($catName, $parent, $template) {

  global $modx;
  
  $products = array();
  
  $dir = 'images/' . $catName;
  
  if ($handle = opendir($dir)) {
	  while (false !== ($entry = readdir($handle))) {
		  if ($entry != "." && $entry != "..") {
			  preg_match('#(?<code>.*)-(?<price>\d+)-(?<name>.*)\.jpg#', $entry, $matches);
			  $product = array(
				  'code' => $matches['code'],
				  'price' => $matches['price'],
				  'name' => $matches['name'],
				  'image' => $dir . '/' . $entry,
			  );
			  $products[] = $product;
		  }
	  }
	  closedir($handle);
  }
  
  $content = '';

  if (!empty($products)) {
	  $oldChilds = $modx->getChildIds($parent);
	  foreach ($oldChilds as $id) {
		  $doc = $modx->getObject('modResource', $id);
		  $doc->remove();
	  }
  }
  
  foreach ($products as $product) {
	  $doc = $modx->newObject('modDocument');
  
	  $doc->set('parent', $parent);
	  $doc->set('pagetitle', $product['name']);
	  $doc->set('alias', $product['code']);
	  $doc->set('template', $template);
	  $doc->set('published', 1);
	  $doc->setContent($content);
	  $doc->save();
	  $id = $doc->get('id');
	  setTV($id, 'price', $product['price']);
	  setTV($id, 'article', $product['code']);
	  setTV($id, 'image', copyImage($id, $product['image']));

  }

}

//uploadCat('торты детские', 31, 9);
//uploadCat('торты свадебные', 32, 9);
//uploadCat('букеты', 27, 9);


//uploadCat('торты мини', 30, 9);
//uploadCat('наборы', 28, 9);
//uploadCat('на пасху', 1055, 9);
//uploadCat('новый год', 21, 9);
//uploadCat('хеллоуин', 29, 9);
//uploadCat('шоколадные', 596, 9);
//uploadCat('необычные', 25, 9);
//uploadCat('с логотипом', 24, 9);
//uploadCat('свадебные', 23, 9);
//uploadCat('фруктовые', 26, 9);
//uploadCat('детские', 22, 9);

exit;