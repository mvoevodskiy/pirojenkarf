id: 7
source: 1
name: shk_include
description: 'Include snippets from PHP files'
category: Shopkeeper
properties: ''
static: 1
static_file: /core/components/shopkeeper/elements/snippets/include.php

-----

if (file_exists(MODX_BASE_PATH.$file)){
   $o = include MODX_BASE_PATH.$file;
}else{ $o = 'File not found at: '.$file; }
return $o;