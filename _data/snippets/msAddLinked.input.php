id: 138
source: 1
name: msAddLinked.input
category: msAddLinked
properties: 'a:8:{s:3:"tpl";a:7:{s:4:"name";s:3:"tpl";s:4:"desc";s:20:"msaddlinked_prop_tpl";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:21:"tpl.msAddLinked.input";s:7:"lexicon";s:22:"msaddlinked:properties";s:4:"area";s:0:"";}s:7:"product";a:7:{s:4:"name";s:7:"product";s:4:"desc";s:24:"msaddlinked_prop_product";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:1:"0";s:7:"lexicon";s:22:"msaddlinked:properties";s:4:"area";s:0:"";}s:4:"link";a:7:{s:4:"name";s:4:"link";s:4:"desc";s:21:"msaddlinked_prop_link";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:1:"0";s:7:"lexicon";s:22:"msaddlinked:properties";s:4:"area";s:0:"";}s:9:"inputType";a:7:{s:4:"name";s:9:"inputType";s:4:"desc";s:26:"msaddlinked_prop_inputType";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:8:"checkbox";s:7:"lexicon";s:22:"msaddlinked:properties";s:4:"area";s:0:"";}s:11:"priceTarget";a:7:{s:4:"name";s:11:"priceTarget";s:4:"desc";s:28:"msaddlinked_prop_priceTarget";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:6:"#price";s:7:"lexicon";s:22:"msaddlinked:properties";s:4:"area";s:0:"";}s:15:"priceOrigTarget";a:7:{s:4:"name";s:15:"priceOrigTarget";s:4:"desc";s:32:"msaddlinked_prop_priceOrigTarget";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:20:"#msal_price_original";s:7:"lexicon";s:22:"msaddlinked:properties";s:4:"area";s:0:"";}s:9:"fieldName";a:7:{s:4:"name";s:9:"fieldName";s:4:"desc";s:26:"msaddlinked_prop_fieldName";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:9:"pagetitle";s:7:"lexicon";s:22:"msaddlinked:properties";s:4:"area";s:0:"";}s:13:"toPlaceholder";a:7:{s:4:"name";s:13:"toPlaceholder";s:4:"desc";s:30:"msaddlinked_prop_toPlaceholder";s:4:"type";s:13:"combo-boolean";s:7:"options";a:0:{}s:5:"value";b:0;s:7:"lexicon";s:22:"msaddlinked:properties";s:4:"area";s:0:"";}}'
static_file: core/components/msaddlinked/elements/snippets/snippet.input.php

-----

/** @var array $scriptProperties */
/** @var msAddLinked $msAddLinked */
if (!$msAddLinked = $modx->getService('msaddlinked', 'msAddLinked', $modx->getOption('msaddlinked_core_path', null, $modx->getOption('core_path') . 'components/msaddlinked/') . 'model/msaddlinked/', $scriptProperties)) {
	return 'Could not load msAddLinked class!';
}
/** @var pdoFetch $pdoFetch */
$pdoFetch = $modx->getService('pdoFetch');

$tpl = $modx->getOption('tpl', $scriptProperties, 'tpl.msAddLinked.input');
//$tplOuter = $modx->getOption('tplOuter', $scriptProperties, 'tpl.msAddLinked.inputOuter');
$product_id = $modx->getOption('product', $scriptProperties, 0);
$link_id = $modx->getOption('link', $scriptProperties, 0);
$input_type = $modx->getOption('inputType', $scriptProperties, 'checkbox');
$field_name = $modx->getOption('fieldName', $scriptProperties, 'pagetitle');
$priceTarget = $modx->getOption('priceTarget', $scriptProperties, '#price');
$priceOrigTarget = $modx->getOption('priceOrigTarget', $scriptProperties, '#msal_price_original');
$toPlaceholder = $modx->getOption('toPlaceholder', $scriptProperties, false);

$var = $modx->getOption('msal_variable', null, 'msal');
$price = 0;
$output = '';

$product_id = $product_id == 0 ? $modx->resource->id : $product_id;

if ($product = $modx->getObject('msProduct', (int) $product_id)) {
    $price = $product->get('price');
} else return;

$criteria = array('master' => $product_id);
if (!is_numeric($link_id)) {
    if ($link = $modx->getObject('msLink', array('name' => $link_id ))) {
        $link_id = $link->get('id');
    } else {
        $link = 0;
    }
};
if ($link_id) {
    $criteria['link'] = $link_id;
}


/** @var msProductLink[] $rows */
$links = $modx->getCollection('msProductLink', $criteria);
$inputs = array();
foreach ($links as $l) {
    /** @var msProduct $linked */
    if ($linked = $l->getOne('Slave')) {
        if ($input_type != 'radio') {
            $value = '';
            $id = $linked->get('id');
        } else {
            $id = 'radio';
            $value = $linked->get('id');
        }
        $inputs[] = array(
            "linked_id" => $id,
            "linked_name" => $linked->get($field_name),
            "linked_price" => $linked->get('price'),
            "link_id" => $l->get('link'),
            "field_name" => $field_name,
            "input_type" => $input_type,
            "value" => $value,
        );
    }
}
if (!empty($inputs)) {
//    $output = implode("\n", $outputArray);
    $output = $pdoFetch->getChunk(
        $tpl,
        array("inputs" => $inputs, "product_price" => $price, "var" => $var),
        true
    );
}
if ($js = trim($modx->getOption('msal_frontend_js'))) {
    if (!empty($js) && preg_match('/\.js/i', $js)) {
        $modx->regClientScript(preg_replace(array('/^\n/', '/\t{7}/'), '', '
                    <script type="text/javascript">
                        var msal = {}; msal.price_target="'. $priceTarget. '";
                        msal.price_orig_target="'.$priceOrigTarget.'";
                    </script>
                    '), true);
        $modx->regClientScript(str_replace('[[+jsUrl]]', '/assets/components/msaddlinked/js/', $js));
    }
}

return $output;