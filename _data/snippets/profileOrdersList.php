id: 127
source: 1
name: profileOrdersList
properties: 'a:2:{s:8:"tplOuter";a:7:{s:4:"name";s:8:"tplOuter";s:4:"desc";s:0:"";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:27:"tpl.profileOrdersList.outer";s:7:"lexicon";N;s:4:"area";s:0:"";}s:6:"tplRow";a:7:{s:4:"name";s:6:"tplRow";s:4:"desc";s:0:"";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:25:"tpl.profileOrdersList.row";s:7:"lexicon";N;s:4:"area";s:0:"";}}'

-----

$tplRow = $modx->getOption('tplRow', $scriptProperties, 'tpl.profileOrdersList.row');
$tplOuter = $modx->getOption('tplOuter', $scriptProperties, 'tpl.profileOrdersList.outer');

$output = '';
$orderRows = '';

if ($user = $modx->getAuthenticatedUser()) {
    $q = $modx->newQuery('msOrder');
    $q->where(array('user_id' => $user->get('id')));
    $q->sortby('id', 'DESC');
    $orders = $modx->getCollection('msOrder', $q);
    foreach($orders as $order) {
        $orderInfo = $order->toArray();
        if ($address = $order->getOne('Address')) {
            $addressInfo = $address->toArray();
            unset($addressInfo['id']);
            $orderInfo = array_merge($orderInfo, $addressInfo);
        }
        if ($delivery = $order->getOne('Delivery')) {
            $orderInfo['delivery'] = $delivery->get('name');
        }
        $orderInfo['products'] = array();
        $products = $order->getMany('Products');
        foreach($products as $product) {
            $productInfo = $product->toArray();
            $productInfo['uri'] = $modx->makeUrl($productInfo['product_id']);
            $parParName = $modx->runSnippet('pdoField', array('id' => $productInfo['product_id'], 'topLevel' => 1,'field' => 'pagetitle'));
            if ($parParName == 'Торты') {
                $parParName = 'Торт';
            }
            $productInfo['name'] = $parParName . ' "' . $productInfo['name'] . '"'; 
            $orderInfo['products'][] = $productInfo;
        }
        $orderInfo['fullInfo'] = json_encode($orderInfo);
        $orderRows .= $modx->getChunk($tplRow, $orderInfo);
    }
    $output = $modx->getChunk($tplOuter, array('orders' => $orderRows));
}

return $output;