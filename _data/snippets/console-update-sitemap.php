id: 91
name: _console_update_sitemap
properties: null

-----

$modx->setLogLevel(xPDO::LOG_LEVEL_ERROR);
$ids = $modx->runSnippet('pdoResources', array('templates' => '19,9,20,17,18,11,14,10', 'returnIds' => 1, 'limit' => 3000,'offset' => 0));
var_dump($ids);

if($stock = $modx->getCollection('msProduct', array('id:IN' => explode(",",$ids)))) {
    foreach($stock as $resource) {
        $properties = $resource->getProperties('stercseo');
        if(empty($properties)){
            echo 'id: '.$resource->get('id')."<br>";
        	$properties = array(
        		'index' => $modx->getOption('stercseo.index', null, '1'),
        		'follow' => $modx->getOption('stercseo.follow', null, '1'),
        		'sitemap' => $modx->getOption('stercseo.sitemap', null, '1'),
        		'priority' => $modx->getOption('stercseo.priority', null, '0.5'),
        		'changefreq' => $modx->getOption('stercseo.changefreq', null, 'weekly'),
        		//'urls' => $modx->fromJSON($_POST['urls'])
        	);
        } else {
            echo 'id with stercseo: '.$resource->get('id')."<br>";
            if(empty($properties['index'])) {
                $properties['index'] = $modx->getOption('stercseo.index', null, '1');
                echo 'new index: '."<br>";
            }
            if(empty($properties['follow'])) {
                $properties['follow'] = $modx->getOption('stercseo.follow', null, '1');
                echo 'new follow: '."<br>";
            }
            if(empty($properties['sitemap'])) {
                $properties['sitemap'] = $modx->getOption('stercseo.sitemap', null, '1');
                echo 'new sitemap: '."<br>";
            }
            if(empty($properties['priority']) || $properties['priority'] == '1') {
                //$properties['priority'] = $modx->getOption('stercseo.priority', null, '0.5');
                //echo 'new priority: '."<br>";
            }
            if(empty($properties['changefreq']) || $properties['changefreq'] == '1') {
                $properties['changefreq'] = $modx->getOption('stercseo.changefreq', null, 'weekly');
                echo 'new changefreq: '."<br>";
            }
            echo 'urls with stercseo: '.print_r($properties['urls'])."<br>";
            $resource->setProperties($properties,'stercseo');
            //$resource->save();
        }
    }
}