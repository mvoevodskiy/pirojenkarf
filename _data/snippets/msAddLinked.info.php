id: 139
source: 1
name: msAddLinked.info
category: msAddLinked
properties: 'a:3:{s:3:"tpl";a:7:{s:4:"name";s:3:"tpl";s:4:"desc";s:20:"msaddlinked_prop_tpl";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:20:"tpl.msAddLinked.info";s:7:"lexicon";s:22:"msaddlinked:properties";s:4:"area";s:0:"";}s:6:"option";a:7:{s:4:"name";s:6:"option";s:4:"desc";s:23:"msaddlinked_prop_option";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:2:"{}";s:7:"lexicon";s:22:"msaddlinked:properties";s:4:"area";s:0:"";}s:9:"fieldName";a:7:{s:4:"name";s:9:"fieldName";s:4:"desc";s:26:"msaddlinked_prop_fieldName";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:9:"pagetitle";s:7:"lexicon";s:22:"msaddlinked:properties";s:4:"area";s:0:"";}}'
static_file: core/components/msaddlinked/elements/snippets/snippet.info.php

-----

/** @var array $scriptProperties */
$tpl = $modx->getOption('tpl', $scriptProperties, 'tpl.msAddLinked.info');
$option = $modx->getOption('option', $scriptProperties, '{}');
$field_name = $modx->getOption('fieldName', $scriptProperties, 'pagetitle');
$output = '';

$pdoFetch = $modx->getService('pdoFetch');
$modx->lexicon->load('msaddlinked:default');

$links = json_decode($option, true);

if (!empty($links)) {
    foreach ($links as $linked => $count) {
        $linked = explode('__', $linked);
        if ($linkedMSP = $modx->getObject('msProduct', (int) $linked[0])) {
            if (((int) $count) <= 1) {
                $count = 1;
            }
            $linksOutput[] = array(
                "linked_id" => $linked[0],
                "linked_name" => $linkedMSP->get($field_name),
                "linked_price" => $linkedMSP->get('price'),
                "linked_count" => $count,
                "link_id" => $linked[1],
                "field_name" => $field_name,
                "value" => '',
            );
        }

    }

    if (!empty($linksOutput)) {
        $output = $pdoFetch->getChunk($tpl, array("links" => $linksOutput), true);
    }
    return $output;
}