id: 87
name: _console_sendemail
properties: null

-----

$props = array(
    'debug' => '0',
    'hideOutput' => '0',
    'message' => 'Some Message',
    'subject' => 'Some Subject',
    'to' => 'saleningrado@mail.ru',
    'fromName' => 'Some Name',
    'emailSender' => 'saleningrado@mail.ru',
    'replyTo' => 'saleningrado@mail.ru',
    'html' => '1',
    'failureMessage' => '<br /><h3 style=&quot;color:red&quot;>Mail Failed</h3>',
    'successMessage' => '<br /><h3 style =&quot;color:green&quot;>Mail reported successful</h3>',
    'errorHeader' => '<br />Mail error:',
    'smtpErrorHeader' => '<br />SMTP server report:',
);

$output =  $modx->runSnippet('QuickEmail',$props);
echo $output;