id: 72
source: 1
name: getCountChildren
properties: 'a:0:{}'

-----

if ($input and is_numeric($input) and $input >= 0) return $modx->getCount('modResource', array('parent' => $input));
else return false;