id: 59
source: 1
name: ms2binder
properties: 'a:0:{}'

-----

// $modx->log(1,'POST: '.print_r($_POST,1));

if (isset($_POST['pir_action']) and $action = $_POST['pir_action']) {
    $ms2 = $modx->getService('minishop2ext', 'miniShop2Ext', $modx->getOption('core_path').'components/minishop2/model/minishop2/', $scriptProperties);
    if (!($ms2 instanceof miniShop2Ext)) {
        return '';
    }

    $ms2->initialize($modx->context->key, array('json_response' => true));

    switch ($action) {
        case 'order/submit':
            $individual = $_POST['individual'];
            if ($individual == 1) {
                $modx->setPlaceholder('individualOrder', 1);
                $modx->setPlaceholder('individualOrderCount', (int) $_POST['count']);
                $photolinks = array();
                $photopath = array();
                if (!empty($_FILES)) {
                    $generator = $modx->newObject('modResource');
                    foreach ($_FILES as $key => $fileindividual) {
                        $fileindividual['name'] = 'order-'.uniqid().'-'.$generator->cleanAlias($fileindividual['name']);
                        $_FILES[$key]['name'] = $fileindividual['name'];
                        $photolinks[] = '<img src="'.$modx->getOption('site_url').'image/orders/'.$fileindividual['name'].'">';
                        $photopath[] = 'image/orders/'.$fileindividual['name'];
                    }
                    if ($source = $modx->getObject('modMediaSource', 1)) {
                        $source->initialize();
                        $source->uploadObjectsToContainer('image/orders/', $_FILES);
                    }
                }
                $comment = "<br>\n".$_POST['comment']."<br>\nПрикрепленные фотографии:<br>\n".implode("<br>\n", $photolinks);
                /*$processorProps = array(
                	'pagetitle' => 'Инд. заказ от '.$_POST["name"].' от '. date('d.m.y h:i', time()),
                	'parent' => 2483,
                	'content' =>  $comment,
                	'class_key' => 'msProduct'
                );*/

                /*$response = $modx->runProcessor('resource/create', $processorProps);


            	if ($response->isError()) {
            		$modx->log(modX::LOG_LEVEL_ERROR, "New individual order (New item on site): Error on $action: \n". print_r($response->getAllErrors(), 1));
            	} else {
            	    $idnewitem = $response->response['object']['id'];
                    $ms2->cart->add((int)$idnewitem, (int) $_POST['count'], array('info' =>'individual', 'attachments' => $photopath));
            	}*/

                $pagetitle_product = 'Инд. заказ от '.$_POST['name'].' от '.date('d.m.y h:i', time());
                $props_product = array(
                    'pagetitle' => $pagetitle_product,
                    'parent' => 2483,
                    'content' => $comment,
                    'class_key' => 'msProduct',
                    'searchable' => 0,
                    'published' => 0,
                );

                $new_item = $modx->newObject('msProduct', $props_product);
                $new_item->set('alias', $new_item->cleanAlias($pagetitle_product));
                if ($new_item->save()) {
                    $idnewitem = $new_item->get('id');
                    $modx->cacheManager->refresh();
                    $cm = $modx->getCacheManager();
                    $cm->refresh();
                    $ms2->cart->add((int) $idnewitem, (int) $_POST['count'], array('info' => 'individual', 'attachments' => $photopath));
                } else {
                    $error = $modx->errorCode();
                    $modx->log(modX::LOG_LEVEL_ERROR, "New individual order (New item on site): Error on $action: \n".$error);
                }
                $delivery = 2;
                if (isset($_POST['delivery_check']) and $_POST['delivery_check'] == 'true') {
                    $delivery = 1;
                }
                $_POST['comment'] = $comment;
                $orderData = array(
                    'email' => $_POST['email'],
                    'delivery' => $delivery,
                    'receiver' => $_POST['name'],
                    'phone' => $_POST['phone'],
                    'comment' => $_POST['comment'],
                    'street' => $_POST['delivery_address'],
                    'city' => $_POST['delivery_date'],
                );
            } else {
                /*
                $products = json_decode($_POST['products'], 1);
                if (!is_array($products)) {
                    return 0;
                }
                $ms2->cart->clean();
                // $modx->log(1, '[JSON DECODE BEGIN]');

                // $modx->log(1, 'post_products: '. print_r($_POST['products'], 1));
                // $modx->log(1, 'products: '.print_r($products, 1));
                // $modx->log(1, 'totalCountProductsOfType: '.print_r(totalCountProductsOfType($products, 'cupcake'), 1));
                // $modx->log(1, 'discountOnGroupProducts: '.print_r(discountOnGroupProducts($products), 1));

                $products = discountOnGroupProducts($products);
                
                
               // $modx->log(modX::LOG_LEVEL_ERROR,'Корзина: '. json_encode($products,true) );

                foreach ($products as $product) {
                    // $modx->log(1, print_r($product, 1));
                    $options = array(
                        'Тесто' => $product['dough'],
                        'Начинка' => $product['stuffing'],
                        'Информация' => $product['info'],
                        'dough' => $product['dough'],
                        'stuffing' => $product['stuffing'],
                        'info' => $product['info'],
                        'price_cart' => $product['price'],
                        'upperprice' => $product['upperprice'],
                    );
                    if (!empty($product)) {
                        $res = $ms2->cart->add((int) $product['id'], (int) $product['count'], $options);
                    }

                    // $modx->log(1, print_r($res, 1));
                }
                
                // $modx->log(1, '[/JSON DECODE END]');

                $delivery = 2;
                if (isset($_POST['delivery_check']) and $_POST['delivery_check'] == 'true') {
                    $delivery = 1;
                }
                */
                
               // $modx->log( 1, 'TEST' );
                
                //$modx->log(1, print_r(  $ms2->cart->get(), 1) );
                
                
                $orderData = array(
                    'email' => $_POST['email'],
                    'delivery' => $delivery,
                    'receiver' => $_POST['name'],
                    'phone' => $_POST['phone'],
                    'comment' => $_POST['comment'],
                    'street' => $_POST['delivery_address'],
                    'city' => $_POST['delivery_date'],
                );
            }

            $result = json_decode($ms2->order->submit($orderData), 1);
             $modx->log(1, print_r($result, 1));

            return $result['data']['msorder'];

            break;
    }
}

return;

/**
 * Считает общее кол-во товаров в корзине определённого типа.
 *
 * @param array  $products
 * @param string $type
 *
 * @return int
 */
function totalCountProductsOfType($products, $type)
{
    $totalCount = 0;

    foreach ($products as $product) {
        if (empty($product)) {
            continue;
        }

        if ($type == 'cupcake' && $product['type'] == 'cupcake' && !$product['isSets']) {
            $totalCount += $product['count'];
        }
    }

    return $totalCount;
}

/**
 * Высчитывает надценку на капкейки и подставляет новые цены товарам
 *
 * @param array $products
 *
 * @return array Обработанный массив $products
 */
function discountOnGroupProducts($products)
{
    $step_price = array(50, 30, 20, 10);
    $step_count = array(6, 9, 13, 24);
    $totalPrice = 0;
    $totalCount = 0;

    $totalCount = totalCountProductsOfType($products, 'cupcake');

    foreach ($products as &$product) {
        if (empty($product)) {
            continue;
        }

        if ($product['type'] == 'cupcake' && !$product['isSets']) {
            if ($totalCount < $step_count[0]) {
                $product['upperprice'] = $step_price[0];
            } elseif ($totalCount < $step_count[1]) {
                $product['upperprice'] = $step_price[1];
            } elseif ($totalCount < $step_count[2]) {
                $product['upperprice'] = $step_price[2];
            } elseif ($totalCount < $step_count[3]) {
                $product['upperprice'] = $step_price[3];
            } else {
                $product['upperprice'] = 0;
            }
        }
    }

    return $products;
}