id: 23
source: 1
name: handleHomePage
properties: 'a:0:{}'

-----

//echo '[[*id]]';

if ($modx->resource->get('id') == '1') {
	return $modx->getChunk('generateContentHome');
}

elseif (in_array($modx->resource->get('id'), array(13,1286,1353,1371,1482,1241,))) {
	return $modx->getChunk('generateContentWithCategories');
}

else {
	return $modx->getChunk('generateContentDefault');
}