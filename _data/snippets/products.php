id: 33
source: 1
name: products
properties: 'a:0:{}'

-----

?>
[[pdoResources?
	&parents=`[[*id]]`
    &depth=`0`
	&limit=`100`
    &tpl=`product_[[*productType]]`
    &includeTVs=`image,price,price_cupcake,price_cakes,productType`
	&sortbyTV=`article`
]]

<?php

return;