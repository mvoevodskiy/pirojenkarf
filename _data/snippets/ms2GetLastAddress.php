id: 137
name: ms2GetLastAddress
category: ms2
properties: 'a:0:{}'

-----

$id = 0;
if ( $modx->user->isAuthenticated() ) {
    $id = $modx->user->get('id');
}
if( $id == 0 ) return;


$q = $modx->newQuery( 'msOrderAddress',
                array(
                    'user_id' => $id
                   ,'street:!=' => ''
                )
);
$q->groupby('street');
/*$q->prepare();
echo $q->toSQL();*/


$total = $modx->getCount('msOrderAddress', $q);
//echo $total;

if( $total > 0 && $tpl != 1 ) {
    $q->sortby( 'createdon', 'DESC' );  // последний берем
    $q->limit( 1 );
    
    $result = $modx->getObject( 'msOrderAddress', $q );
    
    if( $result->get('street') != '' ) {
        return $result->get('street');
    }
} else if( $total > 0 && $tpl == 1 ) {
    
    $array = $modx->getCollection( 'msOrderAddress', $q );
    $output = '';
    foreach($array as $res){
        $output .= '<option>'.$res->get('street').'</option>';
    }
        
    return $output;
}

return;