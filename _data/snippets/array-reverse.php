id: 109
name: array_reverse
properties: 'a:0:{}'

-----

if (!is_array($input)) {
    return $input;
}
$input = array_reverse($input);
return $modx->toJSON($input);