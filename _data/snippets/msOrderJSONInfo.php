id: 60
source: 1
name: msOrderJSONInfo
properties: 'a:0:{}'

-----

if (isset($_REQUEST['msorder']) and $order = $modx->getObject('msOrder', (int) $_REQUEST['msorder'])) {
    
    
    
    $data = array(
        'transactionId' => $order->get('id'),
        'transactionAffiliation' => 'пироженка',
        'transactionTotal' => $order->get('cost'),
        'transactionTax' => 0,
        'transactionShipping' => $order->get('delivery_cost'),
        'transactionProducts' => array(),
        'event' => 'trackTrans',
        
    
    /**
    'transactionProducts': [{
        'sku': 'артикул_товара1',
        'name': 'название_товара1',
        'category': 'категория_товара1',
        'price': цена_товара1,
        'quantity': количество_товаров1
    },{
        'sku': 'артикул_товара2',
        'name': 'название_товара2',
        'category': 'категория_товара2',
        'price': цена_товара2,
        'quantity': количество_товаров2
    }],
     */
     
    );
    $products = $order->getMany('Products');
    foreach($products as $product) {
        if ($origProduct = $product->getOne('Product')) {
            $cat = $modx->runSnippet('pdoField', array('id' => $origProduct->get('id'), 'topLevel' => 1));
            if ($cat == 'Торты') $cat = 'Торт';
            $data['transactionProducts'][] = array(
                'sku' => $origProduct->get('article'),
                'name' => $product->get('name'),
                'price' => $product->get('price'),
                'quantity' => $product->get('count'),
                'category' => $cat
            );
        }
    }
    return json_encode($data);
}

return;