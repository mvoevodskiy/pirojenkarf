id: 6
source: 1
name: num_format
description: 'Number format output filter'
category: Shopkeeper
properties: ''
static: 1
static_file: /core/components/shopkeeper/elements/snippets/snippet.num_format.php

-----

/*
 * numFormat snippet
 * example: [[*price:num_format]]
 */

if(strlen($input)==0) return '';

$input = floatval(str_replace(array(' ',','), array('','.'), $input));
return number_format($input,(floor($input) == $input ? 0 : 2),'.',' ');