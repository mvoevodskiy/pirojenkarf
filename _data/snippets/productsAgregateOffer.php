id: 145
source: 1
name: productsAgregateOffer
properties: 'a:0:{}'

-----

$query = $modx->query('SELECT min(p.price) as minPrice, max(p.price) as maxPrice
    FROM business_ms2_products p
    JOIN business_site_content sc ON sc.id=p.id
    JOIN business_site_content sc2 ON sc2.id=sc.parent
    WHERE sc.parent='.$id.' OR sc2.parent='.$id.'
');
if ($query) {
    $row = $query->fetch(PDO::FETCH_ASSOC);
    return $modx->getChunk('agregateOffer', [
        'min' => $row['minPrice'],
        'max' => $row['maxPrice'],
        'categoryName' => $categoryName,
        'categoryDescription' => $categoryDescription
    ]);
} else {
    return '';
}