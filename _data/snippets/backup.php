id: 45
property_preprocess: 1
name: backup
description: 'Create a SQL dump of your MODX database.'
category: Databackup
properties: "a:12:{s:10:\"dataFolder\";a:7:{s:4:\"name\";s:10:\"dataFolder\";s:4:\"desc\";s:31:\"prop_databackup.dataFolder_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:23:\"[[++databackup.folder]]\";s:7:\"lexicon\";s:21:\"databackup:properties\";s:4:\"area\";s:0:\"\";}s:10:\"tempFolder\";a:7:{s:4:\"name\";s:10:\"tempFolder\";s:4:\"desc\";s:31:\"prop_databackup.tempFolder_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:21:\"[[++databackup.temp]]\";s:7:\"lexicon\";s:21:\"databackup:properties\";s:4:\"area\";s:0:\"\";}s:5:\"purge\";a:7:{s:4:\"name\";s:5:\"purge\";s:4:\"desc\";s:26:\"prop_databackup.purge_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:22:\"[[++databackup.purge]]\";s:7:\"lexicon\";s:21:\"databackup:properties\";s:4:\"area\";s:0:\"\";}s:13:\"includeTables\";a:7:{s:4:\"name\";s:13:\"includeTables\";s:4:\"desc\";s:34:\"prop_databackup.includeTables_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:21:\"databackup:properties\";s:4:\"area\";s:0:\"\";}s:13:\"excludeTables\";a:7:{s:4:\"name\";s:13:\"excludeTables\";s:4:\"desc\";s:34:\"prop_databackup.excludeTables_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:21:\"databackup:properties\";s:4:\"area\";s:0:\"\";}s:9:\"writeFile\";a:7:{s:4:\"name\";s:9:\"writeFile\";s:4:\"desc\";s:30:\"prop_databackup.writeFile_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:21:\"databackup:properties\";s:4:\"area\";s:0:\"\";}s:15:\"writeTableFiles\";a:7:{s:4:\"name\";s:15:\"writeTableFiles\";s:4:\"desc\";s:36:\"prop_databackup.writeTableFiles_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:21:\"databackup:properties\";s:4:\"area\";s:0:\"\";}s:13:\"commentPrefix\";a:7:{s:4:\"name\";s:13:\"commentPrefix\";s:4:\"desc\";s:34:\"prop_databackup.commentPrefix_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:3:\"-- \";s:7:\"lexicon\";s:21:\"databackup:properties\";s:4:\"area\";s:0:\"\";}s:13:\"commentSuffix\";a:7:{s:4:\"name\";s:13:\"commentSuffix\";s:4:\"desc\";s:34:\"prop_databackup.commentSuffix_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:21:\"databackup:properties\";s:4:\"area\";s:0:\"\";}s:7:\"newLine\";a:7:{s:4:\"name\";s:7:\"newLine\";s:4:\"desc\";s:28:\"prop_databackup.newLine_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"\n\";s:7:\"lexicon\";s:21:\"databackup:properties\";s:4:\"area\";s:0:\"\";}s:7:\"useDrop\";a:7:{s:4:\"name\";s:7:\"useDrop\";s:4:\"desc\";s:28:\"prop_databackup.useDrop_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:21:\"databackup:properties\";s:4:\"area\";s:0:\"\";}s:14:\"createDatabase\";a:7:{s:4:\"name\";s:14:\"createDatabase\";s:4:\"desc\";s:35:\"prop_databackup.createDatabase_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:21:\"databackup:properties\";s:4:\"area\";s:0:\"\";}}"

-----

// http://www.phpclasses.org/browse/file/33388.html



$path = $modx->getOption('core_path').'components/databackup/';

// version 5.3+

require_once $path.'model/mysql/dbbackup.class.php';

// require_once $path.'model/mysql/dbbackup5.2.class.php';



$output = '';

// back up my modx database:

$data_folder = $modx->getOption('dataFolder', $scriptProperties, $modx->getOption('databackup.folder', null, $path.'dumps'.DIRECTORY_SEPARATOR));



// added 1.7

$temp_folder = $modx->getOption('tempFolder', $scriptProperties, $modx->getOption('databackup.temp', null, $data_folder.'temp'.DIRECTORY_SEPARATOR));



$purge = $modx->getOption('purge', $scriptProperties, $modx->getOption('databackup.purge', null, 1814400));

// includeTables should be a comma separtaed list

$includeTables = $modx->getOption('includeTables', $scriptProperties, NULL);

// excludeTables should be a comma separtaed list

$excludeTables = $modx->getOption('excludeTables', $scriptProperties, NULL);



$write_file = $modx->getOption('writeFile', $scriptProperties, true);

if ( $write_file === 'false' ) {

    $write_file = false;

    $output .= ' <br>Do not write main file<br>';

}

$write_table_files = $modx->getOption('writeTableFiles', $scriptProperties, true);

if ( $write_table_files === 'false' ) {

    $write_table_files = false;

    $output .= ' <br>Do not write table files<br>';

}

// these are to change how the data file is written

$comment_prefix = $modx->getOption('commentPrefix', $scriptProperties, '-- ');

$comment_suffix = $modx->getOption('commentSuffix', $scriptProperties, '');

$new_line = $modx->getOption('newLine', $scriptProperties, "\n");

// use the sql drop command

$use_drop = $modx->getOption('useDrop', $scriptProperties, true);

if ( $use_drop === 'false' ) {

    $use_drop = false;

}

$database = $modx->getOption('database', $scriptProperties, $modx->getOption('dbname'));

// use the sql create database command

$create_database = $modx->getOption('createDatabase', $scriptProperties, false);

if ( $create_database === 'false' ) {

    $create_database = false;

}



$db = new DBBackup($modx,

    array(

        'comment_prefix' => $comment_prefix,

        'comment_suffix' => $comment_suffix,

        'new_line' => $new_line,

        'base_path' => $data_folder,

        'temp_path' => $temp_folder,

        'write_file' => $write_file,

        'write_table_files' => $write_table_files,

        'use_drop' => $use_drop,

        'database' => $database,

        'create_database' => $create_database,

        'includeTables' => $includeTables,

        'excludeTables' => $excludeTables,

         

    ));



$backup = $db->backup();

if($backup){

    $output .= 'The MODX data has been backed up';

} else {

    $output .= 'An error has ocurred and MODX did not get backed up correctly: '.$db->getErrors();

}

if ( $purge > 0 ) {

    $db->purge($purge);

}

return $output;



// restore: http://efreedom.com/Question/1-898440/PDO-SQL-Server-RESTORE-DATABASE-Query-Wait-Finished

// $pdo->exec('RESTORE DATABASE [blah] FROM DISK = \'c:\blah.bak\' WITH NOUNLOAD');