id: 133
name: msMiniCartProductsGet
category: ms2
properties: 'a:0:{}'

-----

/* @var array $scriptProperties */
/* @var miniShop2 $miniShop2 */
$miniShop2 = $modx->getService('minishop2');
$miniShop2->initialize($modx->context->key);
/* @var pdoFetch $pdoFetch */
if (!$modx->loadClass('pdofetch', MODX_CORE_PATH . 'components/pdotools/model/pdotools/', false, true)) {return false;}
$pdoFetch = new pdoFetch($modx, $scriptProperties);

$cart = $miniShop2->cart->get();

//$modx->log(1, 'cart: '. json_encode($cart) );


$miniCartIds = array();
foreach ($cart as $key => $item) {
    $cart['mini_cart'][$key] = $item;
    $miniCartIds[] = $item['id']; // добавляем id товаров в корзине для получения tv
}


$output = '';
if( !empty($miniCartIds) ) {
    // функция добавляет картинки и pagetitle
    $cart = $miniShop2->cart->msCartCr8($cart, $miniCartIds);
    
    // пропускаем массив через чанк для получения готового html
    foreach($cart['mini_cart'] as $item )  {
        $output .= $modx->getChunk( 'tpl.msMiniCartNew.one', $item );
    }

}

$cart['mini_cart'] = $output;

return $output;