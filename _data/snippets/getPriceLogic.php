id: 30
source: 1
name: getPriceLogic
properties: 'a:0:{}'

-----

$priceCupcake = (int) $priceCupcake;

if (!empty($price)) {
  if (empty($priceCupcake)) {
  	return "$price <span>руб./кг</span>";
  } else {
	return "$price <span>руб./кг за торт</span><br> + <span class=\"price_cupcake\">$priceCupcake</span> <span>руб. за каждый капкейк</span>";
  }
} else {
	return "+ <span class=\"price_cupcake\">$priceCupcake</span> <span>руб. за каждый капкейк</span>";
}

return "";