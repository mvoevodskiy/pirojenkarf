id: 128
source: 1
name: RecentlyViewed
category: RecentlyViewed
properties: 'a:2:{s:3:"tpl";a:7:{s:4:"name";s:3:"tpl";s:4:"desc";s:23:"recentlyviewed_prop_tpl";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:22:"tpl.RecentlyViewed.row";s:7:"lexicon";s:25:"recentlyviewed:properties";s:4:"area";s:0:"";}s:8:"tplEmpty";a:7:{s:4:"name";s:8:"tplEmpty";s:4:"desc";s:28:"recentlyviewed_prop_tplEmpty";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:24:"tpl.RecentlyViewed.empty";s:7:"lexicon";s:25:"recentlyviewed:properties";s:4:"area";s:0:"";}}'
static_file: core/components/recentlyviewed/elements/snippets/snippet.recentlyviewed.php

-----

/** @var array $scriptProperties */
/** @var RecentlyViewed $RecentlyViewed */
//if (!$RecentlyViewed = $modx->getService('recentlyviewed', 'RecentlyViewed', $modx->getOption('recentlyviewed_core_path', null, $modx->getOption('core_path') . 'components/recentlyviewed/') . 'model/recentlyviewed/', $scriptProperties) {
//    return 'Could not load RecentlyViewed class!';
//}

$tpl = $modx->getOption('tpl', $scriptProperties, 'tpl.RecentlyViewed.row');
$tplEmpty = $modx->getOption('tplEmpty', $scriptProperties, 'tpl.RecentlyViewed.empty');
$element = $modx->getOption('element', $scriptProperties, 'msProducts');
$sessionVar = $modx->getOption('recentlyviewed_session_var', null, 'RecentlyViewed');
$output = '';

if (!empty($_SESSION[$sessionVar])) {
    $scriptProperties['tpl'] = $tpl;
    $scriptProperties['parents'] = '0';
    $scriptProperties['resources'] = implode(',', $_SESSION[$sessionVar]);
    $output = $modx->runSnippet($element, $scriptProperties);
}
if (empty($output)) {
    $output = $modx->getChunk($tplEmpty);
}

return $output;