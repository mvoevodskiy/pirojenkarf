id: 136
name: fastOrder
category: AjaxForm
properties: 'a:0:{}'

-----

$errors = array();

if ( empty( trim( $_POST['phone'] ) ) || trim( $_POST['phone'] ) == '7' ) {
    $errors['phone'] = 'Укажите телефон';
}

if ( empty( trim( $_POST['receiver'] ) ) ) {
    $errors['receiver'] = 'Укажите имя';
}

if ( empty( trim( $_POST['city'] ) ) ) {
    $errors['city'] = 'Укажите дату доставки';
}

if( !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ) {
    $errors['email'] = 'Укажите email';
}

if (count($errors) == 0) {
    $errors = '';
}

if ( !empty( $errors ) ) {
    return $AjaxForm->error( 'В форме содержатся ошибки!', $errors );
} else {
    /*
    // неправильный регистр miniShop2 - должно быть minishop2
    if ($miniShop2 = $modx->getService('miniShop2')) {
        $miniShop2->initialize( $modx->context->key, array('json_response' => true) );
    }*/
    /*
    $scriptProperties = array('json_response' => true);
    $miniShop2 = $modx->getService( 'minishop2','miniShop2', MODX_CORE_PATH . 'components/minishop2/model/minishop2/', $scriptProperties );
    $miniShop2->initialize($modx->context->key, $scriptProperties);*/
    
    
    if ($miniShop2 = $modx->getService('minishop2')) {
        $miniShop2->initialize( $modx->context->key, array( 'json_response' => true ) );
        
        $miniShop2->order->add( 'phone', trim( $_POST['phone'] ) );
        $miniShop2->order->add( 'comment', trim( $_POST['comment'] ) );
        $miniShop2->order->add( 'email', trim( $_POST['email'] ) );
        $miniShop2->order->add( 'city', trim( $_POST['city'] ) );
        $miniShop2->order->add( 'cityDate', trim( $_POST['city'] ) );
        $miniShop2->order->add( 'cityCron', trim( $_POST['city'] ) );
        $miniShop2->order->add( 'delivery', 1 );
        $miniShop2->order->add( 'payment', 1 );
        $miniShop2->order->add( 'receiver', trim( $_POST['receiver'] )  );
        
        //$orderData = $miniShop2->order->get();
        
        $result = json_decode( $miniShop2->order->submit(), true );
        $modx->log(1, 'test result: '. json_encode($result) );
        
        
        if( isset($result['success']) && $result['success'] == false ) {
             return $AjaxForm->error($result['message']);
        }
        
        // дальше только редирект через js: response.data.msorder - номер заказа
    
    }
    
    return $AjaxForm->success( 'Форма успешно отправлена.' );
}