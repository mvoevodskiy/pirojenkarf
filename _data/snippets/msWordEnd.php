id: 131
source: 1
name: msWordEnd
category: ms2
properties: 'a:0:{}'

-----

$num = $input; // число
$scriptProperties['item'] = $options; // наименование
$scriptProperties['end_1'] = 'ов'; // например 45 товаров
$scriptProperties['end_2'] = ''; // например 31 товар
$scriptProperties['end_3'] = 'а'; // например // 2 товара
 
$stri = array( $scriptProperties['end_1'], $scriptProperties['end_2'], $scriptProperties['end_3'] );
$index = $num % 100;
if ( $index >=11 && $index <= 14 ) $index = 0;
else $index = ( $index %= 10 ) < 5 ? ( $index > 2 ? 2 : $index ): 0;
 
return $scriptProperties['item'] . $stri[$index];