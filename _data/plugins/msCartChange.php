id: 43
source: 1
name: msCartChange
description: "обновление кол-ва товара\n+ добавление в корзину"
category: miniShop2
properties: 'a:0:{}'

-----

switch ($modx->event->name) {
    
    case 'msOnSubmitOrder':  // необязательный массив $data с переназначаемыми полями
    
        //$modx->log(1,'data: '. json_encode($data) );
        
        // загружаем класс minishop2
        /*if ($miniShop2 = $modx->getService('minishop2')) {
            $miniShop2->initialize($modx->context->key);
        }*/
                
        if( (int)$_REQUEST['thisIsPresent'] != 1 ) {
            $order->ms2->order->remove('thisIsPresent');
            $order->ms2->order->remove('country');
            $order->ms2->order->remove('index');
        }
        
        if( (int)$_REQUEST['ostavitZapisku'] != 1 ) {
            $order->ms2->order->remove('ostavitZapisku');
            $order->ms2->order->remove('region');
        }
        
        //$modx->log(1,'order: '. json_encode($order->ms2->order->get()) );
        
        
        // минимальная сумма заказа:
        if($cart = $order->ms2->cart->status()) {
            if($cart['total_cost'] < 1000) {
                $modx->event->output('Минимальная сумма товаров для заказа <b>1000</b> руб!');
            } 
        }
        
        // добавляем новый email если нужно
        if ($user = $modx->getAuthenticatedUser('web') ) {
            $profile = $user->getOne('Profile');
            //$modx->log( 1,'mail user: '. $profile->get('email') .'; new mail: '. $order->get()['email'] );
            
            if( $profile->get('email') != ( $newmail = $order->get()['email'] ) ) {
                $order->ms2->order->add('metro', $newmail);
            }
        }
        
        $cart = $order->ms2->cart;
        $cartArr = $cart->get();
        $cartArr = $cart->discountOnGroupProducts( $cartArr );
        $cart->set( $cartArr );
        
       // $modx->log(1,'cart: '. json_encode($cartArr) );
    break; 

    case 'msOnBeforeChangeInCart': // получает $key, $count и $cart
        $cartArr = $cart->get();
        
        
        // обновляем начинки и тесто
        $opts = array( 
             'dough'
            ,'stuffing'
            ,'comment'
            ,'cupcakes_dough'
            ,'cupcakes_stuffing'
            ,'cupcakes_count'
            ,'cupcakes_price'
            ,'berries'
        );
        
        foreach( $opts as   $i => $input ) {
            if( !empty( $_REQUEST['options'][$input] ) ) {
                $cartArr[$key]['options'][$input] = $_REQUEST['options'][$input];
            } else if(  $input == 'comment' ) {
                unset( $cartArr[$key]['options'][$input] );
            } else if ( $input == 'berries' ) {
                unset( $cartArr[$key]['options'][$input] );
                unset( $cartArr[$key]['options']['price_with_berries'] );
            }
        }
        $cart->set( $cartArr );
    break;
}

if ( in_array( $modx->event->name, array('msOnChangeInCart', 'msOnAddToCart', 'msOnRemoveFromCart' ) ) ) {
//        $modx->log(1,'TEST!!!!!!! here' );
        $cartArr = $cart->get();
       
        $cartArr = $cart->discountOnGroupProducts( $cartArr ); // считаем скидку на капкейки + пересчет цен для тортов с капкейками
//        $modx->log(1,'TEST!!!!!!! here'. json_encode($cartArr) );
        $cart->set( $cartArr );
}