id: 27
source: 1
name: ImageThumbOnUpload
properties: 'a:1:{s:1:"q";a:7:{s:4:"name";s:1:"q";s:4:"desc";s:0:"";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:2:"90";s:7:"lexicon";N;s:4:"area";s:0:"";}}'
disabled: 1

-----

switch ($modx->event->name) {
    case 'OnFileManagerUpload':
        foreach($files as $f){
      
            // $modx->log(1, 'file: '.print_r($f,1));
            $addDir = '';
            //if (isset($_POST['p_id']) and isset($_POST['tv_id'])) $addDir = date('Y').'/'.date('m').'/'.$_POST['p_id'].'/';
            $currentdoc = $modx->newObject('modResource');
            $f['name'] = $currentdoc->cleanAlias($f['name']);
            $bases = $source->getBases($directory);
            $fullPath = $bases['pathAbsolute'].ltrim($directory,'/');
            $directory = $source->fileHandler->make($fullPath);
            if(explode("/",$f['type'])[0]=="image"){
                //$arr = $source->toArray();
                //$directory = '';
                //$thms=$modx->fromJSON($thumbs);
                //foreach($thms as $thumb){
                //Directory check
                //$modx->log(MODx::LOG_LEVEL_ERROR,print_r($source->toArray(),true));
                /*if($source){
                    $arr=$source->toArray();
                    $directory=$arr['properties']['basePath']['value'];
                    //if ($source->id == 16) $directory .= $addDir;
                }*/
                /*if (!file_exists(MODX_BASE_PATH.$directory.$thumb['name'])) {
                     mkdir(MODX_BASE_PATH.$directory.$thumb['name'], 0775, true);
                }*/
    
                if (!class_exists('modPhpThumb')) {
                	/** @noinspection PhpIncludeInspection */
                	require MODX_CORE_PATH . 'model/phpthumb/modphpthumb.class.php';
                }
                $phpThumb = new modPhpThumb($modx);
                $phpThumb->initialize();
                // Указываем исходник
                $modx->log(modX::LOG_LEVEL_ERROR, str_replace('//', '/', $directory->getPath().$f['name']));
                $phpThumb->setSourceFilename(str_replace('//', '/', $directory->getPath().$f['name']));
                
                // Выставляем параметры
            
                $phpThumb->setParameter('q', $q);
                
                
                // Генерируем 
                if ($phpThumb->GenerateThumbnail()) {
                	if (!$phpThumb->renderToFile($directory->getPath().$f['name'])) {
                		$modx->log(modX::LOG_LEVEL_ERROR, 'Could not save rendered image to'.$directory->getPath().$f['name']);
                	}
                } else {
                	// Если возникла ошибка - пишем лог работы в журнал MODX
                	$modx->log(modX::LOG_LEVEL_ERROR, print_r($phpThumb->debugmessages, 1));
                }
                //}
            }
        }
        break;
        
    case 'OnMODXInit':
        if (empty($_FILES)) return;
        foreach ($_FILES as &$file) {
            $currentdoc = $modx->newObject('modResource');
            $file['name'] =  str_replace(' ', '-', $currentdoc->cleanAlias($file['name']));
        }
        break;
        
    
 
}