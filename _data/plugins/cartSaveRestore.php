id: 46
source: 1
name: cartSaveRestore
category: miniShop2
properties: 'a:0:{}'

-----

switch($modx->event->name) {
    case 'msOnAddToCart':
    case 'msOnChangeInCart':
    case 'msOnEmptyCart':
    case 'msOnRemoveFromCart':
        if ($user = $modx->getAuthenticatedUser('web') and $ms2 = $modx->getService('minishop2')) {
            $ms2->initialize($modx->context->key);
            $profile = $user->getOne('Profile');
            $extended = $profile->get('extended');
            $extended['ms2cart'] = $ms2->cart->get();
            

          // $modx->log(1,'mail user: '. $profile->get('email') );
            
            $profile->set('extended', $extended);
            $profile->save();
            
        }
        break;
        
    case 'OnWebPageInit':
    case 'OnWebLogin':
        if ( $user = $modx->getAuthenticatedUser('web') and  $ms2 = $modx->getService('minishop2')) {
            $ms2->initialize($modx->context->key);
            $profile = $user->getOne('Profile');
            $extended = $profile->get('extended');
            
            
            if ( isset($extended['ms2cart']) && !empty($extended['ms2cart']) ) {
                $cart = $ms2->cart->get();
                $result = $extended['ms2cart'];
                
                // если корзина у неавторизирована не пуста? слить массив
                if( !empty( $cart ) && $cart != $result && $modx->event->name == 'OnWebLogin' ) {
                    $result = array_merge($cart, $extended['ms2cart']);
                    $result = $ms2->cart->discountOnGroupProducts($result);
                    
                    $extended['ms2cart'] = $result;
                    
                    $profile->set('extended', $extended);
                    $profile->save();
                }
                
                $ms2->cart->set($result);
                $ms2->cart->sortCart();
            }
        }
        break;
}