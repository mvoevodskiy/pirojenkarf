id: 29
source: 1
name: OnWebPageInit
properties: 'a:0:{}'

-----

if ($modx->event->name != 'OnWebPageInit' || !isset($_GET['page'])) { return false; }

if ($_GET['page'] == 0)
{
    // $modx->log(1, 'OnWebPageInit '. $_GET['page']);
    // $modx->log(1, 'OnWebPageInit _REQUEST '. print_r($_REQUEST,1));
    
    
    $alias = $modx->context->getOption('request_param_alias', 'q');
    if (!isset($_REQUEST[$alias])) { return false; }
    
    $query = $_REQUEST[$alias];
    $res_id = $modx->findResource($query);
    
    unset($_GET['page'], $_GET[$alias]);
    
    $modx->sendRedirect($modx->makeUrl($res_id, $modx->context->key, $_GET, 'full'), array('responseCode' => 'HTTP/1.1 301 Moved Permanently'));
}