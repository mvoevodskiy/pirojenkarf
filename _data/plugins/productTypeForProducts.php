id: 32
source: 1
name: productTypeForProducts
description: 'Плагин, для корректировки типа у товаров. Проверяет, какой у родителя товара productType и ставит такой же товару.'
properties: 'a:0:{}'

-----

$tv_id = 17;

switch ($modx->event->name) {
    case "OnDocFormSave":
            if ($resource->class_key == 'msProduct') {
                $parentIds = $modx->getParentIds($resource->id, 10, array('context' => $resource->context_key));
                
                if ($parentIds[0]) {
                    if ($tv_parent = $modx->getObject('modTemplateVarResource', ['tmplvarid' => $tv_id, 'contentid' => $parentIds[0]])) {
                        if ($tv = $modx->getObject('modTemplateVarResource', ['tmplvarid' => $tv_id, 'contentid' => $resource->id])) {
                            $tv->set('value', $tv_parent->value);
                            $tv->save();
                        }
                    }
                }
            }
        break;
}