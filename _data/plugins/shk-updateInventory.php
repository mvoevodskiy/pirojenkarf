id: 4
source: 1
name: shk_updateInventory
description: 'Update inventory data.'
category: Shopkeeper
properties: 'a:3:{s:14:"inventory_tvid";a:7:{s:4:"name";s:14:"inventory_tvid";s:4:"desc";s:0:"";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:1:"0";s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"plugin_status";a:7:{s:4:"name";s:13:"plugin_status";s:4:"desc";s:0:"";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:1:"2";s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"salestat_tvid";a:7:{s:4:"name";s:13:"salestat_tvid";s:4:"desc";s:0:"";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:1:"0";s:7:"lexicon";N;s:4:"area";s:0:"";}}'
disabled: 1
static: 1
static_file: /core/components/shopkeeper/elements/plugins/updateInventory.php

-----

/*
 * shk_updateInventory
 * 
 * Плагин обновляет количество товара на складе при переводе статуса заказа в "Отправлен" (или др.)
 * 
 * OnSHKChangeStatus
 */

$eventName = $modx->event->name;

if(!isset($plugin_status)) $plugin_status = 2;
if(!isset($order_id) || !isset($status) || $status!=$plugin_status) return;

$order = $modx->getObject('SHKorder',array('id'=>$order_id));

if($order){
    
    require_once MODX_CORE_PATH."components/shopkeeper/model/shopkeeper.class.php";
    require_once MODX_CORE_PATH."components/shopkeeper/model/shk_mgr.class.php";
    $SHKmanager = new SHKmanager($modx);
    $SHKmanager->getModConfig();
    
    $purchases = unserialize($order->get('content'));
    $allowed = $order->get('allowed');
    $p_allowed = $SHKmanager->allowedArray($allowed,$purchases);
    
    $SHKmanager->updateInventory($purchases,$p_allowed,$scriptProperties);
    
}

return '';