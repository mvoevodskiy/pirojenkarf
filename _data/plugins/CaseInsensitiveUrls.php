id: 14
source: 1
name: CaseInsensitiveUrls
properties: 'a:0:{}'

-----

switch ($modx->event->name) {
	case 'OnWebPageInit':
	     
	    //$modx->hasPermission();
        $url = $_SERVER['REQUEST_URI'];
        $pieces = explode('?', $url);
        if (!preg_match('/\.txt/', $pieces[0])) {
            if (preg_match('/\.xml/', $pieces[0])) {
            } elseif (preg_match('~[A-Z]~', $pieces[0])) {
                $pieces[0] = strtolower($pieces[0]);
                if (preg_match('~\/\/~', $pieces[0])) {
                    $pieces[0] = str_replace("//", "/", $pieces[0]);
                }
                $url = $pieces[0];
                $url .= isset($pieces[1])? '?' . $pieces[1] : '';
                $modx->sendRedirect($url,0,'REDIRECT_HEADER','HTTP/1.1 301 Moved Permanently');
            } elseif (preg_match('~\/\/~', $pieces[0])) {
                $pieces[0] = str_replace("//", "/", $pieces[0]);
                $url = $pieces[0];
                $url .= isset($pieces[1])? '?' . $pieces[1] : '';
                $modx->sendRedirect($url,0,'REDIRECT_HEADER','HTTP/1.1 301 Moved Permanently');
            } elseif ((!preg_match("/.*\/$/", $url)) and (!preg_match("/.*\/\?{1}.*$/", $url))) {
                if (!preg_match('~html~', $pieces[0])) {
                    $url = $pieces[0]. "/";
                    $url .= isset($pieces[1])? '?' . $pieces[1] : '';
                }
                $modx->sendRedirect($url,0,'REDIRECT_HEADER','HTTP/1.1 301 Moved Permanently');
            }
        }
    break;
}