id: 23
source: 1
name: UrlMsProduct
properties: 'a:0:{}'

-----

switch ($modx->event->name) {
    // case 'OnBeforeDocFormSave':
    //     // $modx->log(1, print_r($data,1));
    //     $modx->error->addField('alias', 'Ошибка!');
    //     $modx->event->output('Ошибка!');
    //     return;
    //     break;
    case 'OnDocFormSave':
        //if ($resource->get('class_key') == 'msProduct') {
            if (trim($resource->get('alias'))) return;
            $e = $modx->Event;
            if($mode == 'new') {
                $docid = $e->params['id'];
                // $modx->log(1, 'mode == new:'. $docid);
            } else {
                $docid = $resource->get('id');
                // $modx->log(1, 'mode == resource:'. $docid);
            }
            $resource->save();
            $resource->set('alias', $resource->cleanAlias($resource->get('pagetitle')));
            if($resource->get('uri_override') == 0) {
                $resource->set('uri_override', 1);
            }
            $parName = $resource->get('alias');
            if($modx->getObject('msProduct', $docid)) {
                $parParName = $modx->runSnippet('pdoField', array('id' => $resource->get('parent'), 'topLevel' => 1,'field' => 'alias'));
                
                // ставим вместо "torty" - "tort"
                $parParName = ($parParName == 'torty')
                    ? 'tort'
                    : $parParName;
                
                // 1 - это старый метод генерации без уникализации алиаса, 0 - это новый
                if (0) {
                    $newuri = $parParName .'-'. $parName ."/";
                }
                else {
                    // уникализируем алиас
                    $newuri = $parParName .'-'. $parName;
                    if ($modx->getCount('modResource', ['id:!=' => $docid, 'uri' => $newuri .'/'])) {
                        $i = 1;
                        do  {
                            ++$i;
                        } while ($modx->getCount('modResource', ['id:!=' => $docid, 'uri' => $newuri .'-'. $i .'/']));
                        
                        if ($i > 1) {
                            $newuri = $newuri .'-'. $i;
                        }
                    }
                    $newuri = $newuri ."/";
                }
                
                // $modx->log(1, 'parParName: '. $parParName );
                // $modx->log(1, 'parName: '. $parName );
                // $modx->log(1, $newuri .' !== '. $resource->get('uri'));
                
                $oldurl = $resource->get('uri');
                if($newuri !== $oldurl) {
                    $resource->set('uri', $newuri);
                    $newProperties = $resource->getProperties('stercseo');
                    $newProperties['urls'][count($newProperties['urls'])]['url'] = $oldurl;
                    $resource->setProperties($newProperties,'stercseo');
                }
                
                
                $resource->save();
                
            } 
        //}
        break;
}