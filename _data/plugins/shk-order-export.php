id: 7
source: 1
name: shk_order_export
description: 'Export Shopkeeper orders to XML'
category: Shopkeeper
properties: 'a:0:{}'
disabled: 1
static: 1
static_file: /core/components/shopkeeper/elements/plugins/shk_order_export.inc.php

-----

/*

Export Shopkeeper orders to XML

OnSHKScriptsLoad

*/

$output = '';
$eventName = $modx->event->name;

if($eventName == 'OnSHKScriptsLoad'){
    
    $output = 'assets/components/shopkeeper/js/mgr/widgets/shk_order_export.js';
    
}

$modx->event->output($output);

return '';