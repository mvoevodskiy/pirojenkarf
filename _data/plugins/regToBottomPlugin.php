id: 48
source: 1
name: regToBottomPlugin
description: 'добавляет чанк extra_scripts перед </body>'
category: miniShop2
properties: 'a:0:{}'

-----

switch ($modx->event->name) {
    
    case 'OnWebPagePrerender':
        $html = $modx->getChunk('extra_scripts');
       // $modx->log(1,'start data: ' );
        
        // Парсинг содержимого чанка
        $modx->parser->processElementTags('', $html, true, false, '[[', ']]', array(), 10);
        $modx->parser->processElementTags('', $html, true, true, '[[', ']]', array(), 10);
        
        
        // если есть body - вставляем
        if (strpos($modx->resource->_output, '</body>') !== false) {
		//	$modx->resource->_output = preg_replace("/(<\/body>)/i", $html . "\n\\1", $modx->resource->_output, true);
			$modx->resource->_output = str_replace(
                '</body>',
                $html."\r\n".'</body>',
                $modx->resource->_output
            );
		}
		
		
    break;

}