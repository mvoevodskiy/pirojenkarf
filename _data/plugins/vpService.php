id: 39
source: 1
name: vpService
category: virtualpage
properties: null
static_file: core/components/virtualpage/elements/plugins/plugin.virtualpage_s.php

-----

$virtualpage = $modx->getService('virtualpage', 'virtualpage', $modx->getOption('virtualpage_core_path', null, $modx->getOption('core_path') . 'components/virtualpage/') . 'model/virtualpage/', $scriptProperties);
if (!($virtualpage instanceof virtualpage)) {
	return '';
}
if (!$virtualpage->active) {
	return '';
}
if (isset($_REQUEST['vp_die']) || isset($modx->event->params['vp_die'])) {
	return '';
}
$eventName = $modx->event->name;
if (method_exists($virtualpage, $eventName)) {
	$virtualpage->$eventName($scriptProperties);
}