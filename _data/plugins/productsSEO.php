id: 21
source: 1
name: productsSEO
properties: 'a:0:{}'

-----

if ($modx->event->name != 'OnDocFormSave') return;
if ($resource) {
    if($mode == 'new') {
        if($res = $modx->getObject('modResource',$_REQUEST['parent'])) {
            $parent = $res->get('parent');
            $docid = $res->get('id');
        }
    } else {
        $parent = $resource->get('parent');
        $docid = $resource->get('id');
    }
    if (!in_array($resource->get('template'), array(9,10,11,12,13,14,17,18,19,20))) return;
    $parName = $modx->runSnippet('pdoField', array('id' => $docid, 'topLevel' => 1));
    if ($parName == 'Торты') $parName = 'Торт';
    $modx->log(1, 'par: '. $parName. ', parPar: '. $parParName);
    $cat = $parName;
    $name = '&laquo;'.$resource->get('pagetitle').'&raquo;';
    $description = 'Заказать '.mb_strtolower($cat).' '.$name.' с доставкой по Москве и Московской области. Натуральные ингредиенты и индивидуальный подход к каждому клиенту.';
    if (trim($resource->get('description')) == '') { 
        $resource->set('description', $description);
    }
    if (trim($resource->getTVValue('title_p')) == '') { 
        $resource->setTVValue('title_p', $parName.' '.$name.' с доставкой по Москве | Пироженка.рф');
    }
    if (trim($resource->getTVValue('H1')) == '') { 
        $resource->setTVValue('H1', $parName.' '.$name);
    }
    $resource->save();
    // $resource->setTVValue('title_p', $parName.' '.$name.' с доставкой по Москве | Пироженка.рф');
    // $resource->setTVValue('H1', $parName.' '.$name);
    
    if (!$type = $resource->getTVValue('productType')) {
        $type = $modx->runSnippet('pdoField', array('field' => 'productType', 'id' => $parent));
    }
    if ($type) $resource->setTVValue('productType', $type);
    $modx->cacheManager->clearCache();
}