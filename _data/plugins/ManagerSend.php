id: 24
source: 1
property_preprocess: 1
name: ManagerSend
properties: 'a:5:{s:5:"debug";a:7:{s:4:"name";s:5:"debug";s:4:"desc";s:0:"";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:1:"0";s:7:"lexicon";N;s:4:"area";s:0:"";}s:20:"debugEmailIndividual";a:7:{s:4:"name";s:20:"debugEmailIndividual";s:4:"desc";s:0:"";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:22:"saleningrado@gmail.com";s:7:"lexicon";N;s:4:"area";s:0:"";}s:19:"debugEmailProvider1";a:7:{s:4:"name";s:19:"debugEmailProvider1";s:4:"desc";s:0:"";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:22:"saleningrado@yandex.ru";s:7:"lexicon";N;s:4:"area";s:0:"";}s:19:"debugEmailProvider2";a:7:{s:4:"name";s:19:"debugEmailProvider2";s:4:"desc";s:0:"";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:44:"saleningrado@gmail.com, saleningrado@mail.ru";s:7:"lexicon";N;s:4:"area";s:0:"";}s:4:"logs";a:7:{s:4:"name";s:4:"logs";s:4:"desc";s:0:"";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:1:"1";s:7:"lexicon";N;s:4:"area";s:0:"";}}'

-----

switch ($modx->event->name) {
    case 'msOnSubmitOrder':
        
        $host = $modx->getOption('http_host');
        //$modx->log(1, "test: ".$host);
        if( $host == 'pir.uborka25.ru' ) {
            $modx->setOption('ms2_email_manager', 'mr.gaserge@ya.ru,deathydespaire@gmail.com');
            // ,pirojenka.rf@gmail.com 
            return;
        }
        
        $debug = isset($scriptProperties['debug']) ? $scriptProperties['debug'] : 0;
        $debugEmailProvider1 = isset($scriptProperties['debugEmailProvider1']) ? $scriptProperties['debugEmailProvider1'] : $modx->getOption('emailsender');
        $debugEmailProvider2 = isset($scriptProperties['debugEmailProvider2']) ? $scriptProperties['debugEmailProvider2'] : $modx->getOption('emailsender');
        $debugEmailIndividual = isset($scriptProperties['debugEmailIndividual']) ? $scriptProperties['debugEmailIndividual'] : $modx->getOption('emailsender');
        $logs = isset($scriptProperties['logs']) ? $scriptProperties['logs'] : 0;
        $miniShop2 = $modx->getService('minishop2');
        $miniShop2->initialize($modx->context->key);
        $cartdata = $miniShop2->cart->get();
        $values = &$modx->event->returnedValues;
        /*$provider = array('1' => 0,'2' => 0,'3' => 0);
        foreach ($cartdata as $rowcart) {
            if($product = $modx->getObject('msProduct', $rowcart['id'])) {
                $items[] = $product->get('pagetitle')." ".$product->get('id');
                if ($tv = $modx->getObject('modTemplateVarResource', array('tmplvarid' => 27, 'contentid' => $product->get('id')))) {
                	$providerval = $tv->get('value');
                }
                if($providerval == 1) {
                    $provider[$providerval]++;
                } else {
                    $provider[$providerval]++;
                }
            }
        }*/
        $proverka = false;
        $prov4 = false;
        $prov4only = true;
        $individual = $modx->getPlaceholder('individualOrder');
        foreach ($cartdata as $rowcart) {
            if($product = $modx->getObject('msProduct', $rowcart['id'])) {
                if($product->get('parent') == '2483') {
                    $individual = true;
                    break;
                }
                if($logs == 1) { $items[] = "name: ".$product->get('pagetitle')." id: ".$product->get('id');}
                $providerval = $modx->runSnippet('pdoField',array('id' => $product->get('id'), 'topLevel' => 1, 'field' => 'provider'));
                if($providerval == 2 or $providerval == 3) {
                    $proverka = true;
                    // break;
                } 
                // else {
                //     $proverka = false;
                // }
                if ($providerval == 4) {
                    $prov4 = true;
                } else {
                    $prov4only = false;
                }
            }
        }
        
        /*
        switch ($x) {
            case 0:
                echo "x=0<br>";
                break;
            case 1:
                echo "x=1<br>";
                break;
            case 2:
                echo "x=2<br>";
                break;
        }*/
        
        if($logs == 1) {$log .= "provider check: ".$providerval."\nitems: ".implode(",\n",$items)."\ndebug enabled: ".$debug."\n";}
        if($individual == true) {
            if($debug == 1) {
                $modx->setOption('ms2_email_manager', $debugEmailIndividual);
            } else {
                $modx->setOption('ms2_email_manager', $modx->getOption('ms2_email_manager_individual'));
            }
            $log .= "provider: 0 (ind)\ndebugemail: ".$modx->getOption('ms2_email_manager');
        } elseif ($prov4) {
            if ($prov4only) {
                $modx->setOption('ms2_email_manager', $modx->getOption('ms2_email_manager_provider4'));
                $log .= "provider: 4\ndebugemail: ".$modx->getOption('ms2_email_manager');
            } else {
                $modx->setOption('ms2_email_manager', $modx->getOption('ms2_email_manager_provider_null'));
                $log .= "provider: 0 (4 and other)\ndebugemail: ".$modx->getOption('ms2_email_manager');
            }
        } elseif ($proverka == false) {
            if($debug == 1) {
                $modx->setOption('ms2_email_manager', $debugEmailProvider1);
            } else {
                $modx->setOption('ms2_email_manager', $modx->getOption('ms2_email_manager_provider1'));
            }
            $log .= "provider: 1\ndebugemail: ".$modx->getOption('ms2_email_manager');
        } else {
            if($debug == 1) {
                $modx->setOption('ms2_email_manager', $debugEmailProvider2);
            } else {
                $modx->setOption('ms2_email_manager', $modx->getOption('ms2_email_manager'));
            }
            $log .= "provider: 2\ndebugemail: ".$modx->getOption('ms2_email_manager');
        }
        
        
        
        
        if($logs == 1) {$modx->log(1, "[ORDER BEGIN] ".date('d.m.Y h:i:s', time())."\n".$log."\n[ORDER END]");}
    break;

}

                // $modx->setOption('ms2_email_manager', 'mv@aaa0.ru');