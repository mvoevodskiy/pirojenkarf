id: 28
source: 1
name: msOnBeforeAddToCart
description: 'Устанавливает цену из корзины localStorage'
properties: 'a:0:{}'

-----

switch ($modx->event->name)
{
    case "msOnBeforeAddToCart":
        
        $opts = is_array($options)
            ? $options
            : $modx->fromJSON($options);
        
        $price = !empty($opts['price_cart'])
            ? $opts['price_cart']
                + (!empty($opts['upperprice'])
                    ? (int)$opts['upperprice']
                    : 0)
            : $product->price;
        
        if ($product->price < $price)
        {
            $product->set('price', $price);
        }
        
    break;
}