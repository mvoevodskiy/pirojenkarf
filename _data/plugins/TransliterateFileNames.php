id: 26
source: 1
name: TransliterateFileNames
properties: 'a:0:{}'

-----

switch ($modx->event->name) {
    case 'OnFileManagerUpload':
        $generator = $modx->newObject('modResource');
        $bases = $source->getBases($directory);
        $fullPath = $bases['pathAbsolute'].ltrim($directory,'/');
        $directory = $source->fileHandler->make($fullPath);
        foreach ($files as $file) {
            $ext = @pathinfo($file['name'],PATHINFO_EXTENSION);
            //exit($ext);
            if(in_array($ext, array('gif','jpeg','jpg','png', 'GIF','JPEG','JPG','PNG'))) {
                if (!class_exists('modPhpThumb')) {
            	/** @noinspection PhpIncludeInspection */
        			require MODX_CORE_PATH . 'model/phpthumb/modphpthumb.class.php';
        		}
        		$phpThumb = new modPhpThumb($modx);
        		$phpThumb->initialize();
    
                // Указываем исходник
                $phpThumb->setSourceFilename($directory->getPath().$file['name']);
                
                // Выставляем параметры
                //foreach ($thumb['props'] as $k => $v) {
                	$phpThumb->setParameter('q', '80');
                //}
                $image_info = getimagesize($directory->getPath().$file['name']);
                if($image_info[0] > 900) {
                    if(abs(100-floor($image_info[1]/($image_info[0]/100))) < 10) {
                        $phpThumb->setParameter('w', '575');
                        $phpThumb->setParameter('h', '575');
                    }
                }
                // Генерируем уменьшенную копию
                if ($phpThumb->GenerateThumbnail()) {
                	if (!$phpThumb->renderToFile($directory->getPath().$file['name'])) {
                		$modx->log(modX::LOG_LEVEL_ERROR, 'Could not save rendered image to'.MODX_BASE_PATH.$directory->getPath().$file['name']);
                	}
                } else {
                	// Если возникла ошибка - пишем лог работы в журнал MODX
                	$modx->log(modX::LOG_LEVEL_ERROR, print_r($phpThumb->debugmessages, 1));
                }
                /*print_r($ext);
                print_r($file['name']);
                print_r($generator->cleanAlias($file['name']));
                print_r(str_replace($ext, '.'.$ext, $generator->cleanAlias($file['name'])));
                exit();*/
                rename($directory->getPath().$file['name'], $directory->getPath() .
                str_replace($ext, $ext, $generator->cleanAlias($file['name'])));
            }
        }
        break;
    default: break;
}
return true;