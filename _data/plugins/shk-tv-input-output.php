id: 6
source: 1
name: shk_tv_input_output
description: 'Print Shopkeeper`s input and output types for TV.'
category: Shopkeeper
properties: 'a:0:{}'
disabled: 1
static: 1
static_file: /core/components/shopkeeper/elements/plugins/tv_input_output.php

-----

$corePath = MODX_CORE_PATH.'components/shopkeeper/';

switch ($modx->event->name) {
    //tv input
    case 'OnTVInputRenderList':
        if($modx->context->get('key') != 'mgr') return;
        $modx->regClientStartupScript($modx->getOption('assets_url').'components/shopkeeper/js/mgr/shk_mgr.js');
        $modx->regClientStartupScript($modx->getOption('assets_url').'components/shopkeeper/js/mgr/widgets/shk.grid.js');
        $modx->regClientCSS($modx->getOption('assets_url').'components/shopkeeper/css/mgr.css');
        $modx->event->output($corePath.'elements/tv/input/');
    break;
    case 'OnTVInputPropertiesList':
        if($modx->context->get('key') != 'mgr') return;
        $modx->event->output($corePath.'elements/tv/inputproperties/');
    break;
    //tv output
    case 'OnTVOutputRenderList':
        $modx->event->output($corePath.'elements/tv/output/');
    break;
    case 'OnTVOutputRenderPropertiesList':
        $modx->event->output($corePath.'elements/tv/outputproperties/');
    break;
}

return;