<?php
define('MODX_API_MODE', true);

do {
    $dir = dirname(!empty($file) ? dirname($file) : __FILE__);
    $file = $dir.'/index.php';
    $i = isset($i) ? --$i : 10;
} while ($i && !file_exists($file));

if (!file_exists($file)) {
    exit('[Ошибка] Не удалось найти MODX!');
}
require_once $file;

// Включаем обработку ошибок
$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget(XPDO_CLI_MODE ? 'ECHO' : 'HTML');

// ID ТВ поля "productType"
$tv_id = 17;

// Список категорий с типами
$cats_list = [
    12 => [
        'type' => 'cupcake',
        'exclude' => [],
    ],
    13 => [
        'type' => 'cakes',
        'exclude' => [30],
    ],
    30 => [
        'type' => 'mini-cakes',
        'exclude' => [],
    ],
    1286 => [
        'type' => 'macarons',
        'exclude' => [],
    ],
    1353 => [
        'type' => 'cakepops',
        'exclude' => [],
    ],
    1371 => [
        'type' => 'imbirnoe-pechenye',
        'exclude' => [],
    ],
    1482 => [
        'type' => 'candy-bar',
        'exclude' => [],
    ],
    1241 => [
        'type' => 'box',
        'exclude' => [],
    ],
];

// Проходимся по полученным ресурсам, у которых неверный тип, и проставляем
$output = '';
foreach ($cats_list as $id => $data) {
    if ($type_errors = getCats($tv_id, $id, $data['type'], $data['exclude'])) {
        $output .= $data['type'].PHP_EOL;
        // $output .= print_r($type_errors,1);
        
        foreach ($type_errors as $key => $resources) {
            if ($key == 'cats') {
                $output .= 'count cats '. count($resources).PHP_EOL;
                // $output .= print_r($resources,1);
            }
            if ($key == 'products') {
                $output .= 'count products '. count($resources).PHP_EOL;
                // $output .= print_r($resources,1);
            }
            foreach ($resources as $res_id => $res_data) {
                if ($tv = $modx->getObject('modTemplateVarResource', ['tmplvarid' => $tv_id, 'contentid' => $res_id])) {
                    $tv->set('value', $data['type']);
                    $tv->save();
                }
            }
        }
        $output .= PHP_EOL;
    }
}

echo '<textarea style="width: 100%; height: 400px; overflow: auto;">'.$output.'</textarea>';

/**
 * [getCats description]
 * @param  [type]  $tv_id       [description]
 * @param  integer $parent      [description]
 * @param  string  $type        [description]
 * @param  array   $exclude     [description]
 * @param  boolean $only_childs [description]
 * @return [type]               [description]
 */
function getCats($tv_id, $parent = 0, $type = '', $exclude = array(), $only_childs = false) {
    global $modx;
    if (!$tv_id || !$type) {
        return;
    }
    $errors = [];
    $output = [];
    $where = ['class_key' => 'msCategory'];
    
    if (!$only_childs) {
        $where += ['id' => $parent];
        if ($cat = $modx->getObject('msCategory', $where)) {
            // $output[$cat->id] = [
            //     'id' => $cat->id,
            // ];
            $errors['cats'][$cat->id] = [];
            
            // Получаем тип для подчинённых товаров
            if ($tvs = $cat->TemplateVarResources) {
                foreach ($tvs as $tv) {
                    if ($tv->tmplvarid == $tv_id) {
                        if ($tv->value != $type) {
                            $errors['cats'][$cat->id] += [
                                'type' => $tv->value,
                            ];
                        }
                        // $output[$cat->id] += [
                        //     'type' => $tv->value,
                        // ];
                    }
                }
            }
            
            // Получаем товары
            if ($products = $modx->getIterator('msProduct', ['class_key' => 'msProduct', 'parent' => $cat->id])) {
                foreach ($products as $p => $product) {
                    if (is_object($product)) {
                        // $output[$cat->id]['products'][$p] = [
                        //     'id' => $product->id,
                        // ];
                        $errors['products'][$product->id] = [];
                        
                        // Получаем тип товара
                        if ($tvs = $product->TemplateVarResources) {
                            foreach ($tvs as $t => $tv) {
                                if ($tv->tmplvarid == $tv_id) {
                                    if ($tv->value != $type) {
                                        $errors['products'][$product->id] += [
                                            'type' => $tv->value,
                                        ];
                                    }
                                    // $output[$cat->id]['products'][$p] += [
                                    //     'type' => $tv->value,
                                    // ];
                                }
                            }
                        }
                    }
                }
            }
            
            $errors += getCats($tv_id, $parent, $type, $exclude, true);
        }
        
        return array_clean($errors);
    }
    
    $where += ['parent' => $parent];
    if ($exclude) {
        $where += ['id:NOT IN' => $exclude];
    }
    
    if ($cats = $modx->getIterator('msCategory', $where)) {
        foreach ($cats as $c => $cat) {
            if (is_object($cat)) {
                // $output[$cat->id] = [
                //     'id' => $cat->id,
                // ];
                $errors['cats'][$cat->id] = [];
                
                // Получаем тип для подчинённых товаров
                if ($tvs = $cat->TemplateVarResources) {
                    foreach ($tvs as $t => $tv) {
                        if ($tv->tmplvarid == $tv_id) {
                            if ($tv->value != $type) {
                                $errors['cats'][$cat->id] += [
                                    'type' => $tv->value,
                                ];
                            }
                            // $output[$cat->id] += [
                            //     'type' => $tv->value,
                            // ];
                        }
                    }
                }
                
                // Рекурсируем...
                if ($subcats = getCats($tv_id, $cat->id, $type, $exclude, true)) {
                    $errors['cats'][$cat->id] += $subcats;
                }
                
                // Получаем товары
                if ($products = $modx->getIterator('msProduct', ['class_key' => 'msProduct', 'parent' => $cat->id])) {
                    foreach ($products as $p => $product) {
                        if (is_object($product)) {
                            // $output[$cat->id]['products'][$p] = [
                            //     'id' => $product->id,
                            // ];
                            $errors['products'][$product->id] = [];
                            
                            // Получаем тип товара
                            if ($tvs = $product->TemplateVarResources) {
                                foreach ($tvs as $t => $tv) {
                                    if ($tv->tmplvarid == $tv_id) {
                                        if ($tv->value != $type) {
                                            $errors['products'][$product->id] += [
                                                'type' => $tv->value,
                                            ];
                                        }
                                        // $output[$cat->id]['products'][$p] += [
                                        //     'type' => $tv->value,
                                        // ];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    return array_clean($errors);
}

/**
 * [array_clean description]
 * @param  [type] $array [description]
 * @return [type]        [description]
 */
function array_clean($array)
{
    if (!is_array($array)) {
        return false;
    }
    foreach ($array as $k => $v) {
        if (is_array($v)) {
            $array[$k] = array_clean($v);
            if (count($array[$k]) == false) {
                unset($array[$k]);
            }
        } else {
            if ($v === '' || $v === null || $v == false || empty($v)) {
                unset($array[$k]);
            }
        }
    }
    return $array;
}