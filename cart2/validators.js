/**
 * Created by slava on 7/13/16.
 */

$(document).ready(function(){
    
    
    $('input.quantityNumOnly').keypress(function (e) {
        var code = e.charCode || e.keyCode; // e.wich
        if(
            code != 8  // backspace
            && code != 46 // delete
            && code != 39 // right arrow
            && code != 37 // left arrow
            && code != 48 // 0
            && code != 49 // 1
            && code != 50 // 2
            && code != 51 // 3
            && code != 52 // 4
            && code != 53 // 5
            && code != 54 // 6
            && code != 55 // 7
            && code != 56 // 8
            && code != 57 // 9
        //&& code != 9
        ){ return false; }
    });
    
    // make pretty scroll
    $(document).on('click', 'button.scrollTo, a.scrollTo', function(){
        var target = $(this).attr('href');
        if( typeof(target) == 'undefined' ) {
            var target = $(this).data('href')
        }
        
        var now = $(window).scrollTop();
        var to =  $(target).offset().top;

        //alert(now+' to '+to)
        var distance = Math.abs( now-to );
        var offset = parseInt( typeof( $(target).attr('data-menu-offset') !== "undefined" ) ? $(target).attr('data-menu-offset') : '0' );
        var speed = parseInt( (distance > 2000) ? '800' : (distance*0.5) );

        if( isNaN(offset) ) {
            var offset = 0;
        }
        $.smoothScroll({scrollTarget: target, offset: offset, speed: speed});

        return false;
    });
    
    
    /*
    var maskList = $.masksSort([{ "mask": "+7(###)###-##-##" }], ['#'], /[0-9]|#/, "mask");
    window.maskOpts = {
        inputmask: {
            definitions: {
                '#': {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            //clearIncomplete: true,
            showMaskOnHover: false,
            autoUnmask: true,
            onUnMask: function (maskedValue, unmaskedValue) {
                
                $(document).find('input[name="phone"]').removeClass('error').parents('.input-parent').removeClass('error');
                    
                var count = unmaskedValue.length
                if( count == 11 ) {
                    $(document).find('input[name="phone"]').parents('.input-parent').addClass('statusOk');
                } else {
	                $(document).find('input[name="phone"]').parents('.input-parent').removeClass('statusOk');
                }
            
                return unmaskedValue;
              }
        },
        match: /[0-9]/,
        replace: '#',
        list: maskList,
        listKey: "mask",
    };
$('.phone_mask').inputmasks(window.maskOpts);*/
 // $(".phone_mask").mask("+7 (999) 999-99-99");  

    
});


(function($){
    $(function() {

        $('span.jQtooltip').each(function() {
            var el = $(this);
            var title = el.attr('title');
            if (title && title != '') {
                el.attr('title', '').append('<div style="width: auto; height: auto;">' + title + '</div>');
                var width = el.find('div').width();
                var height = el.find('div').height();
                el.hover(
                    function() {
                        el.find('div')
                            .clearQueue().show();
                    },
                    function() {
                        el.find('div').hide();
                    }
                ).mouseleave(function() {
                    if (el.children().is(':hidden')) el.find('div').clearQueue();
                });
            }
        })

    })
})(jQuery)
