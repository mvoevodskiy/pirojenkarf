<?php

$settings = array();

$tmp = array(
	'session_var' => array(
		'xtype' => 'textfield',
		'value' => 'RecentlyViewed',
		'area' => 'recentlyviewed_main',
	),
	'max_store_count' => array(
		'xtype' => 'textfield',
		'value' => '10',
		'area' => 'recentlyviewed_main',
	),
	'filter' => array(
		'xtype' => 'textfield',
		'value' => '{"class_key":"msProduct"}',
		'area' => 'recentlyviewed_main',
	),

);

foreach ($tmp as $k => $v) {
	/* @var modSystemSetting $setting */
	$setting = $modx->newObject('modSystemSetting');
	$setting->fromArray(array_merge(
		array(
			'key' => 'recentlyviewed_' . $k,
			'namespace' => PKG_NAME_LOWER,
		), $v
	), '', true, true);

	$settings[] = $setting;
}

unset($tmp);
return $settings;
