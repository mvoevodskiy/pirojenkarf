<?php
include_once 'setting.inc.php';

$_lang['recentlyviewed'] = 'RecentlyViewed';
$_lang['recentlyviewed_menu_desc'] = 'A sample Extra to develop from.';
$_lang['recentlyviewed_intro_msg'] = 'You can select multiple items by holding Shift or Ctrl button.';

$_lang['recentlyviewed_empty'] = 'You haven\'t browsed other items yet.';