<?php
/** @var array $scriptProperties */
/** @var RecentlyViewed $RecentlyViewed */
//if (!$RecentlyViewed = $modx->getService('recentlyviewed', 'RecentlyViewed', $modx->getOption('recentlyviewed_core_path', null, $modx->getOption('core_path') . 'components/recentlyviewed/') . 'model/recentlyviewed/', $scriptProperties) {
//    return 'Could not load RecentlyViewed class!';
//}

$tpl = $modx->getOption('tpl', $scriptProperties, 'tpl.RecentlyViewed.row');
$tplEmpty = $modx->getOption('tplEmpty', $scriptProperties, 'tpl.RecentlyViewed.empty');
$element = $modx->getOption('element', $scriptProperties, 'msProducts');
$sessionVar = $modx->getOption('recentlyviewed_session_var', null, 'RecentlyViewed');
$output = '';

if (!empty($_SESSION[$sessionVar])) {
    $scriptProperties['tpl'] = $tpl;
    $scriptProperties['parents'] = '0';
    $scriptProperties['resources'] = implode(',', $_SESSION[$sessionVar]);
    $output = $modx->runSnippet($element, $scriptProperties);
}
if (empty($output)) {
    $output = $modx->getChunk($tplEmpty);
}

return $output;