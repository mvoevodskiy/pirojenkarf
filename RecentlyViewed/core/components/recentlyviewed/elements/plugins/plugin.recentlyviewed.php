<?php

switch ($modx->event->name) {
    case 'OnWebPagePrerender':

        $sessionVar     = $modx->getOption('recentlyviewed_session_var', null, 'RecentlyViewed');
        $maxStoreCount  = $modx->getOption('recentlyviewed_max_store_count', null, 10);
        $filter         = $modx->getOption('recentlyviewed_filter', null, '{}');

        $recentlyViewed = isset($_SESSION['RecentlyViewed']) ? $_SESSION['RecentlyViewed'] : array();
        $id = $modx->resource->id;

        $filter = json_decode($filter, true);
        $filter = array_merge($filter, array('id' => $modx->resource->id));

        if (!in_array($id, $recentlyViewed) and $modx->getCount('modResource', $filter)) {
            while (count($recentlyViewed) >= $maxStoreCount) {
                array_shift($recentlyViewed);
            }
            $recentlyViewed[] = $modx->resource->id;
            $_SESSION['RecentlyViewed'] = $recentlyViewed;
        }

}