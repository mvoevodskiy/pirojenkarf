<?php

/**
 * Class RecentlyViewedMainController
 */
abstract class RecentlyViewedMainController extends modExtraManagerController {
	/** @var RecentlyViewed $RecentlyViewed */
	public $RecentlyViewed;


	/**
	 * @return void
	 */
	public function initialize() {
		$corePath = $this->modx->getOption('recentlyviewed_core_path', null, $this->modx->getOption('core_path') . 'components/recentlyviewed/');
		require_once $corePath . 'model/recentlyviewed/recentlyviewed.class.php';

		$this->RecentlyViewed = new RecentlyViewed($this->modx);
		$this->addCss($this->RecentlyViewed->config['cssUrl'] . 'mgr/main.css');
		$this->addJavascript($this->RecentlyViewed->config['jsUrl'] . 'mgr/recentlyviewed.js');
		$this->addHtml('
		<script type="text/javascript">
			RecentlyViewed.config = ' . $this->modx->toJSON($this->RecentlyViewed->config) . ';
			RecentlyViewed.config.connector_url = "' . $this->RecentlyViewed->config['connectorUrl'] . '";
		</script>
		');

		parent::initialize();
	}


	/**
	 * @return array
	 */
	public function getLanguageTopics() {
		return array('recentlyviewed:default');
	}


	/**
	 * @return bool
	 */
	public function checkPermissions() {
		return true;
	}
}


/**
 * Class IndexManagerController
 */
class IndexManagerController extends RecentlyViewedMainController {

	/**
	 * @return string
	 */
	public static function getDefaultController() {
		return 'home';
	}
}