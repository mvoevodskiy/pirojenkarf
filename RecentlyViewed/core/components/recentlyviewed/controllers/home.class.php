<?php

/**
 * The home manager controller for RecentlyViewed.
 *
 */
class RecentlyViewedHomeManagerController extends RecentlyViewedMainController {
	/* @var RecentlyViewed $RecentlyViewed */
	public $RecentlyViewed;


	/**
	 * @param array $scriptProperties
	 */
	public function process(array $scriptProperties = array()) {
	}


	/**
	 * @return null|string
	 */
	public function getPageTitle() {
		return $this->modx->lexicon('recentlyviewed');
	}


	/**
	 * @return void
	 */
	public function loadCustomCssJs() {
		$this->addCss($this->RecentlyViewed->config['cssUrl'] . 'mgr/main.css');
		$this->addCss($this->RecentlyViewed->config['cssUrl'] . 'mgr/bootstrap.buttons.css');
		$this->addJavascript($this->RecentlyViewed->config['jsUrl'] . 'mgr/misc/utils.js');
		$this->addJavascript($this->RecentlyViewed->config['jsUrl'] . 'mgr/widgets/items.grid.js');
		$this->addJavascript($this->RecentlyViewed->config['jsUrl'] . 'mgr/widgets/items.windows.js');
		$this->addJavascript($this->RecentlyViewed->config['jsUrl'] . 'mgr/widgets/home.panel.js');
		$this->addJavascript($this->RecentlyViewed->config['jsUrl'] . 'mgr/sections/home.js');
		$this->addHtml('<script type="text/javascript">
		Ext.onReady(function() {
			MODx.load({ xtype: "recentlyviewed-page-home"});
		});
		</script>');
	}


	/**
	 * @return string
	 */
	public function getTemplateFile() {
		return $this->RecentlyViewed->config['templatesPath'] . 'home.tpl';
	}
}