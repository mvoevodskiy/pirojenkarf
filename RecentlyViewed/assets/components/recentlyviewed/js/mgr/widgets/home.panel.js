RecentlyViewed.panel.Home = function (config) {
	config = config || {};
	Ext.apply(config, {
		baseCls: 'modx-formpanel',
		layout: 'anchor',
		/*
		 stateful: true,
		 stateId: 'recentlyviewed-panel-home',
		 stateEvents: ['tabchange'],
		 getState:function() {return {activeTab:this.items.indexOf(this.getActiveTab())};},
		 */
		hideMode: 'offsets',
		items: [{
			html: '<h2>' + _('recentlyviewed') + '</h2>',
			cls: '',
			style: {margin: '15px 0'}
		}, {
			xtype: 'modx-tabs',
			defaults: {border: false, autoHeight: true},
			border: true,
			hideMode: 'offsets',
			items: [{
				title: _('recentlyviewed_items'),
				layout: 'anchor',
				items: [{
					html: _('recentlyviewed_intro_msg'),
					cls: 'panel-desc',
				}, {
					xtype: 'recentlyviewed-grid-items',
					cls: 'main-wrapper',
				}]
			}]
		}]
	});
	RecentlyViewed.panel.Home.superclass.constructor.call(this, config);
};
Ext.extend(RecentlyViewed.panel.Home, MODx.Panel);
Ext.reg('recentlyviewed-panel-home', RecentlyViewed.panel.Home);
