var RecentlyViewed = function (config) {
	config = config || {};
	RecentlyViewed.superclass.constructor.call(this, config);
};
Ext.extend(RecentlyViewed, Ext.Component, {
	page: {}, window: {}, grid: {}, tree: {}, panel: {}, combo: {}, config: {}, view: {}, utils: {}
});
Ext.reg('recentlyviewed', RecentlyViewed);

RecentlyViewed = new RecentlyViewed();