RecentlyViewed.page.Home = function (config) {
	config = config || {};
	Ext.applyIf(config, {
		components: [{
			xtype: 'recentlyviewed-panel-home', renderTo: 'recentlyviewed-panel-home-div'
		}]
	});
	RecentlyViewed.page.Home.superclass.constructor.call(this, config);
};
Ext.extend(RecentlyViewed.page.Home, MODx.Component);
Ext.reg('recentlyviewed-page-home', RecentlyViewed.page.Home);