<?php
/** @noinspection PhpIncludeInspection */
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php';
/** @noinspection PhpIncludeInspection */
require_once MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php';
/** @noinspection PhpIncludeInspection */
require_once MODX_CONNECTORS_PATH . 'index.php';
/** @var RecentlyViewed $RecentlyViewed */
$RecentlyViewed = $modx->getService('recentlyviewed', 'RecentlyViewed', $modx->getOption('recentlyviewed_core_path', null, $modx->getOption('core_path') . 'components/recentlyviewed/') . 'model/recentlyviewed/');
$modx->lexicon->load('recentlyviewed:default');

// handle request
$corePath = $modx->getOption('recentlyviewed_core_path', null, $modx->getOption('core_path') . 'components/recentlyviewed/');
$path = $modx->getOption('processorsPath', $RecentlyViewed->config, $corePath . 'processors/');
$modx->request->handleRequest(array(
	'processors_path' => $path,
	'location' => '',
));