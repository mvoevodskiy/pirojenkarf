<?php

//if(!isset($_POST) || empty($_POST) || !isset($_POST['name']) || empty($_POST['name'])){
//	return false;
//}
//SR

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

class GA_Parse
{

    var $campaign_source; // Campaign Source
    var $campaign_name; // Campaign Name
    var $campaign_medium; // Campaign Medium
    var $campaign_content; // Campaign Content
    var $campaign_term; // Campaign Term

    var $first_visit; // Date of first visit
    var $previous_visit;// Date of previous visit
    var $current_visit_started;// Current visit started at
    var $times_visited;// Times visited
    var $pages_viewed;// Pages viewed in current session


    function __construct() {
        // If we have the cookies we can go ahead and parse them.
        if (isset($_COOKIE["__utma"]) and isset($_COOKIE["__utmz"])) {
            $this->ParseCookies();
        }

    }

    function ParseCookies(){

        // Parse __utmz cookie
        list($domain_hash,$timestamp, $session_number, $campaign_numer, $campaign_data) = explode('.', $_COOKIE["__utmz"],5);

        // Parse the campaign data
        $campaign_data = parse_str(strtr($campaign_data, "|", "&"));

        $this->campaign_source = $utmcsr;
        $this->campaign_name = $utmccn;
        $this->campaign_medium = $utmcmd;
        if (isset($utmctr)) $this->campaign_term = $utmctr;
        if (isset($utmcct)) $this->campaign_content = $utmcct;

        // You should tag you campaigns manually to have a full view
        // of your adwords campaigns data.
        // The same happens with Urchin, tag manually to have your campaign data parsed properly.

        if (isset($utmgclid)) {
            $this->campaign_source = "google";
            $this->campaign_name = "";
            $this->campaign_medium = "cpc";
            $this->campaign_content = "";
            $this->campaign_term = $utmctr;
        }

        // Parse the __utma Cookie
        list($domain_hash,$random_id,$time_initial_visit,$time_beginning_previous_visit,$time_beginning_current_visit,$session_counter) = explode('.', $_COOKIE["__utma"]);




        $this->first_visit = date("d M Y - H:i",$time_initial_visit);
        $this->previous_visit = date("d M Y - H:i",$time_beginning_previous_visit);
        $this->current_visit_started = date("d M Y - H:i",$time_beginning_current_visit);
        $this->times_visited = $session_counter;

        // Parse the __utmb Cookie

        list($domain_hash,$pages_viewed,$garbage,$time_beginning_current_session) = explode('.', $_COOKIE["__utmb"]);
        $this->pages_viewed = $pages_viewed;



        // End ParseCookies
    }

// End GA_Parse
}

$aux = new GA_Parse();

$ga_str = "<br><br><br>Campaign source: ".$aux->campaign_source."<br>";
$ga_str .= "Campaign name: ".$aux->campaign_name."<br>";
$ga_str .= "Campaign medium: ".$aux->campaign_medium."<br>";
$ga_str .= "Campaign content: ".$aux->campaign_content."<br>";
$ga_str .= "Campaign term: ".$aux->campaign_term."<br>";

$ga_str .= "Date of first visit: ".$aux->first_visit."<br>";
$ga_str .= "Date of previous visit: ".$aux->previous_visit."<br>";
$ga_str .= "Date of current visit: ".$aux->current_visit_started."<br>";
$ga_str .= "Times visited: ".$aux->times_visited."<br>";
$ga_str .= "Pages viewed current visit: ".$aux->pages_viewed."<br>";


$name = strip_tags($_POST['name']);
$phone = strip_tags($_POST['phone']);
$email = strip_tags($_POST['email']);
$comment = strip_tags($_POST['comment']);
$delivery_date = strip_tags($_POST['delivery_date']);
$delivery_address = strip_tags($_POST['delivery_address']);
$delivery_check = strip_tags($_POST['delivery_check']);
$delivery_price = strip_tags($_POST['delivery_price']);
$comment = strip_tags($_POST['comment']);
$total = strip_tags($_POST['total']);
$products = $_POST['products'];

$products = json_decode($products);

$json = file_get_contents('data.json');
$data = json_decode($json);
$data->order_id++;
file_put_contents('data.json', json_encode($data));
$order_id = $data->order_id;

$stocks = array();

// retrieve info
$subject = 'Заказ Пироженка.рф #' . $order_id;
$html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"	"http://www.w3.org/TR/html4/loose.dtd">';
$html .= '<html>';
$html .= '<head>';
$html .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
$html .= '<title>' . $subject . '</title>';
$html .= '</head>';
$html .= '<body>';
$html .= 'Имя: '.$name.'<br>';
$html .= 'Телефон: '.$phone.'<br>';
$html .= 'Email: '.$email.'<br>';
$html .= 'Дата доставки: '.$delivery_date.'<br>';
$html .= 'Адрес доставки: '.$delivery_address.'<br>';
$html .= 'Комментарий: '.$comment.'<br>';

$html .= '<br>';

$html .= '<table style="border-collapse: collapse;">';
$html .= '<tr>
	<th style="border: 1px solid #aaa; padding: 3px;">Категория</th>
	<th style="border: 1px solid #aaa; padding: 3px;">Название</th>
	<th style="border: 1px solid #aaa; padding: 3px;">Фото</th>
	<th style="border: 1px solid #aaa; padding: 3px;">Код</th>
	<th style="border: 1px solid #aaa; padding: 3px;">Тесто</th>
	<th style="border: 1px solid #aaa; padding: 3px;">Начинка/крем</th>
	<th style="border: 1px solid #aaa; padding: 3px;">Доп. инфо</th>
	<th style="border: 1px solid #aaa; padding: 3px;">Кол-во</th>
	<th style="border: 1px solid #aaa; padding: 3px;">Цена</th>
</tr>';
foreach ($products as $product) {
	if ($product == NULL) {
		continue;
	}
	$html .= '<tr>';
	$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;">' . $product->category . '</td>';
	$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;">' . $product->name . '</td>';
	$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;"><img src="http://xn--80ajchmregk.xn--p1ai/' . $product->image . '" alt="" width="150"></td>';
	$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;">' . $product->article . '</td>';
	$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;">' . $product->dough . '</td>';
	$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;">' . $product->stuffing . '</td>';
	$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;">' . ($product->info ? $product->info : '-') . '</td>';
	$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;">' . $product->count . ' шт.</td>';
	$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;">' . ($product->price * $product->count) . ' руб.</td>' ;
	$html .= '</tr>';
	if (empty($product->supplier_id)) {
		$product->supplier_id = 1;
	}
	$stocks[$product->supplier_id] = $product->supplier_id;
}

$html .= '</table>';


$html .= '<br>Итого: ' . $total . ' руб. <br>';

if ($delivery_check == 'true') {
	$html .= 'Доставка: клиент желает сам забрать заказ.';
} else {
	$html .= 'Доставка: ' . $delivery_price . ' руб.';
}

//$html .= '<br><br>Заявка с сайта Пироженка.рф и клиенту надо перезвонить.<br>

//<br>
//Если не сложно, отпишитесь пожалуйста - что заявка получена.<br>
//если клиент отказался или поменял количество или вид капкейков - отпишитесь пожалуйста.';

$html .= '</body>';
$html .= '</html>';

header('Content-Type: text/html; charset=utf-8');

$headers  = "MIME-Version: 1.0\n";
$headers .= "From: <pirojenka.rf@gmail.com>\n";
$headers .= "Content-Type: text/html; charset=utf-8\n";
$headers .= "X-Mailer: PHP/" . phpversion();

//$send_to = '';
//$result = mail($send_to, '=?UTF-8?B?' . base64_encode($subject) . '?=', $html, $headers);

$html .= $ga_str;
//$send_to = 'leo@astyle.org.ua';
//$send_to = 'pirojenka.rf@gmail.com';
//$send_to = 'info@lacupcake.ru, pirojenka.rf@gmail.com';
//$result = mail($send_to, '=?UTF-8?B?' . base64_encode($subject) . '?=', $html, $headers);

/*
if (array_key_exists(2, $stocks)) {
	$result = mail('pirojenka.rf@gmail.com, info@chaudeau.ru', '=?UTF-8?B?' . base64_encode($subject) . '?=', $html, $headers);
} else if (array_key_exists(1, $stocks)) {
	$result = mail('pirojenka.rf@gmail.com, info@lacupcake.ru', '=?UTF-8?B?' . base64_encode($subject) . '?=', $html, $headers);
} else {
	$result = mail('pirojenka.rf@gmail.com', '=?UTF-8?B?' . base64_encode($subject) . '?=', $html, $headers);
}

*/

	if (array_key_exists(2, $stocks) && array_key_exists(1, $stocks)) {
		$result = mail('pirojenka.rf@gmail.com, info@akkanto.ru', '=?UTF-8?B?' . base64_encode($subject) . '?=', $html, $headers);
	}
	elseif (array_key_exists(1, $stocks)) {
		$result = mail('pirojenka.rf@gmail.com, info@lacupcake.ru', '=?UTF-8?B?' . base64_encode($subject) . '?=', $html, $headers);
	}
	elseif (array_key_exists(2, $stocks)) {
		$result = mail('info@akkanto.ru, pirojenka.rf@gmail.com', '=?UTF-8?B?' . base64_encode($subject) . '?=', $html, $headers);
	}


//$result = mail('leo@astyle.org.ua', '=?UTF-8?B?' . base64_encode($subject) . '?=', $html, $headers);


if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
	
	// mail to client

	$headers  = "MIME-Version: 1.0\n";
	$headers .= "From: <pirojenka.rf@gmail.com>\n";
	$headers .= "Content-Type: text/html; charset=utf-8\n";
	$headers .= "X-Mailer: PHP/" . phpversion();

	$subject = 'Заказ в магазине Пироженка.рф #' . $order_id;
	
	
	$html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"	"http://www.w3.org/TR/html4/loose.dtd">';
	$html .= '<html>';
	$html .= '<head>';
	$html .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
	$html .= '<title>' . $subject . '</title>';
	$html .= '</head>';
	$html .= '<body>';
    $html .= 'Здравствуйте, '.$name.'.<br>Мы получили ваш заказ №' . $order_id .'.<br><br>';
    $html .= '<b>Данные заказа:</b> '.$name.'<br>';
    $html .= 'Имя: '.$name.'<br>';
    $html .= 'Телефон: '.$phone.'<br>';
    $html .= 'Email: '.$email.'<br>';
    $html .= 'Дата доставки: '.$delivery_date.'<br>';
    $html .= 'Адрес доставки: '.$delivery_address.'<br>';
    $html .= 'Комментарий: '.$comment.'<br>';

$html .= '<table style="border-collapse: collapse;">';
$html .= '<tr>
		<th style="border: 1px solid #aaa; padding: 3px;">Категория</th>
		<th style="border: 1px solid #aaa; padding: 3px;">Название</th>
		<th style="border: 1px solid #aaa; padding: 3px;">Код</th>
		<th style="border: 1px solid #aaa; padding: 3px;">Тесто</th>
		<th style="border: 1px solid #aaa; padding: 3px;">Начинка/крем</th>
		<th style="border: 1px solid #aaa; padding: 3px;">Доп. инфо</th>
		<th style="border: 1px solid #aaa; padding: 3px;">Кол-во</th>
		<th style="border: 1px solid #aaa; padding: 3px;">Цена</th>
	</tr>';

	foreach ($products as $product) {
		if ($product == NULL) {
			continue;
		}
		$html .= '<tr>';
		$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;">' . $product->category . '</td>';
		$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;">' . $product->name . '</td>';
		$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;">' . $product->article . '</td>';
		$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;">' . $product->dough . '</td>';
		$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;">' . $product->stuffing . '</td>';
		$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;">' . ($product->info ? $product->info : '-') . '</td>';
		$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;">' . $product->count . ' шт.</td>';
		$html .= '<td style="text-align: center; border: 1px solid #aaa; padding: 3px;">' . ($product->price * $product->count) . ' руб.</td>' ;
		$html .= '</tr>';
	}
	
	$html .= '</table>';
	
	$html .= '<br>Итого: ' . $total . ' руб.';
	
	
	if ($delivery_check == 'true') {
		$html .= '<br>Доставка: самовывоз.<br>';
	} else {
		$html .= '<br>Доставка: ' . $delivery_price . ' руб.<br>';
	}

	/* mailtext */

	$mailtext = Array();

	$mailtext[2] = "Наши менеджеры обязательно подтвердят заказ по электронной почте (или по телефону) в самое ближайшее время.<br>Пожалуйста, приготовьте точную сумму, у курьера может не быть сдачи.<br>С уважением, магазин Пироженка.рф";
	$mailtext[1] = "Ваш персональный менеджер Артак Казарян. Он обязательно подтвердит Ваш заказ по электронной почте или по телефону в самое ближайшее время.<br>По любому вопросу, включая изготовление и доставку, вы можете пообщаться с Артаком по тел. +79036258992.<br>Пожалуйста, приготовьте точную сумму, у курьера может не быть сдачи.<br>С уважением, магазин Пироженка.рф";

	if (array_key_exists(2, $stocks) && array_key_exists(1, $stocks)) {
		$html .= $mailtext[2];
	}
	elseif (array_key_exists(1, $stocks)) {
		$html .= $mailtext[1];
	}
	elseif (array_key_exists(2, $stocks)) {
		$html .= $mailtext[2];
	}

	/**/
	
	$html .= '</body>';
	$html .= '</html>';
	

	$result = mail($email, '=?UTF-8?B?' . base64_encode($subject) . '?=', $html, $headers);
}

echo $order_id;