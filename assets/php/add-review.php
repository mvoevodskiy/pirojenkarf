<?php


$name = strip_tags($_POST['name']);
$email = strip_tags($_POST['email']);
$proffesion = strip_tags($_POST['proffesion']);
$url = strip_tags($_POST['url']);
$comment = strip_tags($_POST['comment']);
$city = 'Москва';

if (!empty($_FILES)) {
	$tmp_file = $_FILES['file']['tmp_name'];
	$filename = $_FILES['file']['name'];
}

//$send_to = 'leo@astyle.org.ua';
$send_to = 'pirojenka.rf@gmail.com';
// $send_to = 'mv@aaa0.ru';

// retrieve info
$subject = 'Отзыв Пироженка.рф';
$html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"	"http://www.w3.org/TR/html4/loose.dtd">';
$html .= '<html>';
$html .= '<head>';
$html .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
$html .= '<title>' . $subject . '</title>';
$html .= '</head>';
$html .= '<body>';
$html .= 'На сайте пироженка.рф добавлено новый отзыв.<br><br>';
$html .= 'Имя: '.$name.'<br>';
$html .= 'Email: '.$email.'<br>';
$html .= 'Профессия: '.$proffesion.'<br>';
$html .= 'URL: '.$url.'<br>';
$html .= 'Комментарий: '.$comment.'<br>';

$html .= '<br>';

$html .= '</body>';
$html .= '</html>';

// // a random hash will be necessary to send mixed content
// $separator = md5(time());

// // carriage return type (we use a PHP end of line constant)
// $eol = PHP_EOL;

// // main header (multipart mandatory)
// $headers .= "From: <pirojenka.rf@gmail.com>" . $eol;
// $headers .= "MIME-Version: 1.0" . $eol;
// $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol . $eol;
// $headers .= "Content-Transfer-Encoding: 7bit" . $eol;
// $headers .= "This is a MIME encoded message." . $eol . $eol;

// // message
// $headers .= "--" . $separator . $eol;
// $headers .= "Content-Type: text/html; charset=\"utf-8\"" . $eol;
// $headers .= "Content-Transfer-Encoding: 8bit" . $eol . $eol;
// $headers .= $html . $eol . $eol;


// if ($tmp_file) {

// 	$attachment = chunk_split(base64_encode(file_get_contents($tmp_file)));
	
// 	// attachment
// 	$headers .= "--" . $separator . $eol;
// 	$headers .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . $eol;
// 	$headers .= "Content-Transfer-Encoding: base64" . $eol;
// 	$headers .= "Content-Disposition: attachment" . $eol . $eol;
// 	$headers .= $attachment . $eol . $eol;
// 	$headers .= "--" . $separator . "--";
	
// }

// $result = mail($send_to, '=?UTF-8?B?' . base64_encode($subject) . '?=', '', $headers);
	
header('Content-Type: text/html; charset=utf-8');

require_once '../../config.core.php';
require_once MODX_CORE_PATH.'model/modx/modx.class.php';
$modx = new modX();
$modx->initialize('web');
$modx->getService('error','error.modError', '', '');

        if (!isset($modx->mail) || !is_object($modx->mail)) {
            $modx->getService('mail', 'mail.modPHPMailer');
        }
        $modx->mail->set(modMail::MAIL_FROM, $modx->getOption('emailsender'));
        $modx->mail->set(modMail::MAIL_FROM_NAME, $modx->getOption('site_name'));
        $modx->mail->setHTML(true);
        $modx->mail->set(modMail::MAIL_SUBJECT, trim($subject));
        $modx->mail->set(modMail::MAIL_BODY, $html);
        $modx->mail->address('to', trim($send_to));
        if ($tmp_file) {	
            $modx->mail->attach($tmp_file, $filename);
        }
        if (!$modx->mail->send()) {
            $modx->log(modX::LOG_LEVEL_ERROR,'An error occurred while trying to send the email: '.$modx->mail->mailer->ErrorInfo);
        }
        $modx->mail->reset();

$doc = $modx->newObject('modDocument');

function copyImage($id, $image) {
	$path = '../../assets/img/reviews/' . $id . '.jpg';
	copy($image, $path);
	return 'assets/img/reviews/' . $id . '.jpg';
}

function setTV($id, $name, $val) {
	global $modx;
  	$tv = $modx->getObject('modTemplateVar', array('name' => $name));
 	$tv->setValue($id, $val);
    $tv->save();
}

$parent = 72;

$doc->set('parent', $parent);
$doc->set('pagetitle', $name);
$doc->set('alias', md5(rand()));
$doc->set('template', 5);
$doc->set('published', 0);
$doc->setContent('<p>' . $comment . '</p>');
$doc->save();
$id = $doc->get('id');
setTV($id, 'information', $city);
setTV($id, 'vk', $url);
setTV($id, 'proffesion', $proffesion);
if ($tmp_file) {
	setTV($id, 'img', copyImage($id, $tmp_file));
}