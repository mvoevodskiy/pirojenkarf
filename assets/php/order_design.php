<?php

//if(!isset($_POST) || empty($_POST) || !isset($_POST['name']) || empty($_POST['name'])){
//	return false;
//}
//SR
class GA_Parse
{

    var $campaign_source; // Campaign Source
    var $campaign_name; // Campaign Name
    var $campaign_medium; // Campaign Medium
    var $campaign_content; // Campaign Content
    var $campaign_term; // Campaign Term

    var $first_visit; // Date of first visit
    var $previous_visit;// Date of previous visit
    var $current_visit_started;// Current visit started at
    var $times_visited;// Times visited
    var $pages_viewed;// Pages viewed in current session


    function __construct() {
        // If we have the cookies we can go ahead and parse them.
        if (isset($_COOKIE["__utma"]) and isset($_COOKIE["__utmz"])) {
            $this->ParseCookies();
        }

    }

    function ParseCookies(){

        // Parse __utmz cookie
        list($domain_hash,$timestamp, $session_number, $campaign_numer, $campaign_data) = explode('.', $_COOKIE["__utmz"],5);

        // Parse the campaign data
        $campaign_data = parse_str(strtr($campaign_data, "|", "&"));

        $this->campaign_source = $utmcsr;
        $this->campaign_name = $utmccn;
        $this->campaign_medium = $utmcmd;
        if (isset($utmctr)) $this->campaign_term = $utmctr;
        if (isset($utmcct)) $this->campaign_content = $utmcct;

        // You should tag you campaigns manually to have a full view
        // of your adwords campaigns data.
        // The same happens with Urchin, tag manually to have your campaign data parsed properly.

        if (isset($utmgclid)) {
            $this->campaign_source = "google";
            $this->campaign_name = "";
            $this->campaign_medium = "cpc";
            $this->campaign_content = "";
            $this->campaign_term = $utmctr;
        }

        // Parse the __utma Cookie
        list($domain_hash,$random_id,$time_initial_visit,$time_beginning_previous_visit,$time_beginning_current_visit,$session_counter) = explode('.', $_COOKIE["__utma"]);

        $this->first_visit = date("d M Y - H:i",$time_initial_visit);
        $this->previous_visit = date("d M Y - H:i",$time_beginning_previous_visit);
        $this->current_visit_started = date("d M Y - H:i",$time_beginning_current_visit);
        $this->times_visited = $session_counter;

        // Parse the __utmb Cookie

        list($domain_hash,$pages_viewed,$garbage,$time_beginning_current_session) = explode('.', $_COOKIE["__utmb"]);
        $this->pages_viewed = $pages_viewed;



        // End ParseCookies
    }

// End GA_Parse
}


$aux = new GA_Parse();

$ga_str = "<br><br><br>Campaign source: ".$aux->campaign_source."<br>";
$ga_str .= "Campaign name: ".$aux->campaign_name."<br>";
$ga_str .= "Campaign medium: ".$aux->campaign_medium."<br>";
$ga_str .= "Campaign content: ".$aux->campaign_content."<br>";
$ga_str .= "Campaign term: ".$aux->campaign_term."<br>";

$ga_str .= "Date of first visit: ".$aux->first_visit."<br>";
$ga_str .= "Date of previous visit: ".$aux->previous_visit."<br>";
$ga_str .= "Date of current visit: ".$aux->current_visit_started."<br>";
$ga_str .= "Times visited: ".$aux->times_visited."<br>";
$ga_str .= "Pages viewed current visit: ".$aux->pages_viewed."<br>";


$name = strip_tags($_POST['name']);
$phone = strip_tags($_POST['phone']);
$email = strip_tags($_POST['email']);
$count = strip_tags($_POST['count']);
$date = strip_tags($_POST['date']);
$comment = strip_tags($_POST['comment']);
$delivery_date = strip_tags($_POST['delivery_date']);
$delivery_address = strip_tags($_POST['delivery_address']);
$delivery_check = strip_tags($_POST['delivery_check']);
$delivery_price = strip_tags($_POST['delivery_price']);
$comment = strip_tags($_POST['comment']);


if (!empty($_FILES)) {
    foreach($_FILES as $files){
	   $tmp_files[] = array($files['tmp_name'], $files['name']);
	   $filename[] = $files['name'];
    }
}


$json = file_get_contents('data.json');
$data = json_decode($json);
$data->order_id++;
file_put_contents('data.json', json_encode($data));
$order_id = $data->order_id;

// retrieve info
$subject = 'Заказ Пироженка.рф #' . $order_id;
$html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"	"http://www.w3.org/TR/html4/loose.dtd">';
$html .= '<html>';
$html .= '<head>';
$html .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
$html .= '<title>' . $subject . '</title>';
$html .= '</head>';
$html .= '<body>';
$html .= 'Имя: '.$name.'<br>';
$html .= 'Телефон: '.$phone.'<br>';
$html .= 'Email: '.$email.'<br>';
$html .= 'Дата доставки: '.$delivery_date.'<br>';
$html .= 'Адрес доставки: '.$delivery_address.'<br>';
$html .= 'Комментарий: '.$comment.'<br>';

$html .= '<br>';

$html .= 'Клиент желает индивидуальный дизайн.<br>';
$html .= 'Количество: '.$count.'<br>';


if ($delivery_check == 'true') {
	$html .= '<br>Доставка: клиент желает сам забрать заказ<br>';
} else {
	$html .= '<br>Доставка: ' . $delivery_price . ' руб.<br>';
}

//$html .= '<br><br>Заявка с сайта Пироженка.рф и клиенту надо перезвонить.<br>

//<br>
//Если не сложно, отпишитесь пожалуйста - что заявка получена.<br>
//Если клиент отказался или поменял количество или вид капкейков - отпишитесь пожалуйста.';

$html .= '</body>';
$html .= '</html>';

// a random hash will be necessary to send mixed content
$separator = md5(time());

// carriage return type (we use a PHP end of line constant)
$eol = PHP_EOL;

// main header (multipart mandatory)
$headers .= "From: <pirojenka.rf@gmail.com>" . $eol;
$headers .= "MIME-Version: 1.0" . $eol;
$headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
$headers .= "Content-Transfer-Encoding: 7bit" . $eol;
$headers .= "This is a MIME encoded message." . $eol . $eol;

$html .= $ga_str;
$message = '';
// message
$message .= "--" . $separator . $eol;
$message .= "Content-Type: text/html; charset=\"utf-8\"" . $eol;
$message .= "Content-Transfer-Encoding: 8bit" . $eol . $eol;
$message .= $html . $eol . $eol;

if ($tmp_files) {
    $files_data = '';
    $message .= "--" . $separator . $eol;
    foreach($tmp_files as $tmp_file){
        $file_data = '';
    	$attachment = chunk_split(base64_encode(file_get_contents($tmp_file[0])));
   	 
    	// attachment
    	//$file_data .= "--" . $separator . $eol;
    	$file_data .= "Content-Type: application/octet-stream; name=\"" . $tmp_file[1] . "\"" . $eol;
    	$file_data .= "Content-Transfer-Encoding: base64" . $eol;
    	$file_data .= "Content-Disposition: attachment" . $eol . $eol;
    	$file_data .= $attachment . $eol . $eol;
   	    $file_data .= "--" . $separator.$eol;
        $files_data .= $file_data;
    }
        $message .= $files_data. "--";
    
	
}

//$send_to = 'leo@astyle.org.ua';

$send_to = 'pirojenka.rf@gmail.com';

//$send_to = 'arsen.soroka@gmail.com';
//$send_to = 'info@lacupcake.ru, pirojenka.rf@gmail.com';

ini_set('display_errors', 1);
error_reporting(E_ALL);

$result = mail($send_to, '=?UTF-8?B?' . base64_encode($subject) . '?=', $message, $headers);

if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
	
	// mail to client

	$headers  = "MIME-Version: 1.0\n";
	$headers .= "From: <pirojenka.rf@gmail.com>\n";
	$headers .= "Content-Type: text/html; charset=utf-8\n";
	$headers .= "X-Mailer: PHP/" . phpversion();

	$subject = 'Заказ в магазине Пироженка.рф #' . $order_id;
	
	
	$html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"	"http://www.w3.org/TR/html4/loose.dtd">';
	$html .= '<html>';
	$html .= '<head>';
	$html .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
	$html .= '<title>' . $subject . '</title>';
	$html .= '</head>';
	$html .= '<body>';
	$html .= 'Здравствуйте, '.$name.'.<br>Мы получили ваш заказ №' . $order_id .'.<br><br>';
	
	$html .= 'Наши менеджеры обязательно подтвердят заказ по электронной почте (или по телефону) в самое ближайшее время.<br>';
	$html .= 'Пожалуйста, приготовьте точную сумму, у курьера может не быть сдачи.<br>';
	$html .= "С уважением, магазин Пироженка.рф";
	
	$html .= '</body>';
	$html .= '</html>';
	
	$result = mail($email, '=?UTF-8?B?' . base64_encode($subject) . '?=', $html, $headers);
}


	
header('Content-Type: text/html; charset=utf-8');
	
echo $order_id;