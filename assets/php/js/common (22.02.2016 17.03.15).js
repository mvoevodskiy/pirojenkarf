function buildCatalog() {
	$('.reasons .reason').each(function() {
		var n = $('.products .content > div').size();
		var randomN = 1 + Math.floor(Math.random() * n);	
		var className = $(this).attr('class');
		var idx = className.replace('reason reason', '');
		$('<div class="product reason reason'+idx+'">' + $(this).html() + '</div>').insertBefore('.products .content > div:nth-child('+randomN+')');
		$(this).remove();
	});
	$('.reasons').remove();
}

function homeResize(type) {
	var catalogWidth = $('#' + type).width();
	var minProductWidth = 300;
	var maxProductWidth = 370;
	var realWidth;
	if (catalogWidth <= 600) {
		realWidth = 500;
        var items = 1;
		while (catalogWidth / items > minProductWidth) {
			items++;
		}
		items--;
		realWidth = catalogWidth / items;
		if (realWidth > 500) {
			realWidth = 500;
		}
	} else if(catalogWidth <= 1000) {
		realWidth = minProductWidth;
	} else {
		var items = 1;
		while (catalogWidth / items > minProductWidth) {
			items++;
		}
		items--;
		realWidth = catalogWidth / items;
		if (realWidth > maxProductWidth) {
			realWidth = maxProductWidth;
		}
	}
    
	$('#'+type+' .categories').width(realWidth - 50);
	$('#'+type+' .categories').height(realWidth - 50);
	$('#'+type+' .categories').css('margin', 25);
}
function coolResize() {
	console.log(2222);
	var catalogWidth = $('.products').width();
	var minProductHeight = 30;
	var minProductWidth = 300;
	var maxProductWidth = 450;
	var realWidth;
	if (catalogWidth <= 1000) {
		realWidth = minProductWidth;
	} else {
		var items = 1;
		while (catalogWidth / items > minProductWidth) {
			items++;
		}
		items--;
		realWidth = catalogWidth / items;
		if (realWidth > maxProductWidth) {
			realWidth = maxProductWidth;
		}
	}
    if (catalogWidth <= 600) {
	    $('.product').width(realWidth);
	    $('.product').height(realWidth);
        $('.product.reason').width(realWidth);
        $('.product.reason').height(realWidth);
	} else {
        $('.product').width(realWidth - 50);
        $('.product').height(realWidth - 50);
    }
	//$('.product').css('margin', 20);
	$('.cart-product').height('auto');
}
function coolResizeReviews() {
	var catalogWidth = $('#reviews').width();
	var minProductWidth = 300;
	var maxProductWidth = 370;
	var realWidth;
	if (catalogWidth <= 600) {
		realWidth = 500;
        var items = 1;
		while (catalogWidth / items > minProductWidth) {
			items++;
		}
		items--;
		realWidth = catalogWidth / items;
		if (realWidth > 500) {
			realWidth = 500;
		}
	} else if (catalogWidth <= 1000) {
		realWidth = minProductWidth;
	} else {
		var items = 1;
		while (catalogWidth / items > minProductWidth) {
			items++;
		}
		items--;
		realWidth = catalogWidth / items;
		if (realWidth > maxProductWidth) {
			realWidth = maxProductWidth;
		}
	}
	$('.review').width(realWidth - 50);
	$('.review').height(realWidth - 50);
	$('.review').css('margin', 25);
}
function coolResizeTrust() {
	var catalogWidth = $('#trust-us').width();
	var minProductWidth = 300;
	var maxProductWidth = 370;
	var realWidth;
	if (catalogWidth <= 600) {
		realWidth = 500;
        var items = 1;
		while (catalogWidth / items > minProductWidth) {
			items++;
		}
		items--;
		realWidth = catalogWidth / items;
		if (realWidth > 500) {
			realWidth = 500;
		}
	} else if (catalogWidth <= 1000) {
		realWidth = minProductWidth;
	} else {
		var items = 1;
		while (catalogWidth / items > minProductWidth) {
			items++;
		}
		items--;
		realWidth = catalogWidth / items;
		if (realWidth > maxProductWidth) {
			realWidth = maxProductWidth;
		}
	}
    if (catalogWidth <= 600) {
	    $('#trust-us img').width(realWidth - 50);
	    $('#trust-us img').height(realWidth - 50);
	} else {
        $('#trust-us img').width(realWidth - 50);
        $('#trust-us img').height(realWidth - 50);
    }
	$('#trust-us img').css('margin', 25);
}
function cartUpdateProduct(id, obj) {
	var products = localStorage.getItem('products');
	products = JSON.parse(products);
	products[id].count = $(obj).val();
	if (products[id].type == 'cakes') {
	    products[id].info = products[id].info.replace(/\d кг/, products[id].count + ' кг');
	}
	$(obj).parent().parent().find('.col3').text((products[id].count * products[id].price) + ' руб.');
	localStorage.setItem("products", JSON.stringify(products));
	var total = 0;
	for (k in products) {
		if (products[k] == null) {
			continue;
		}
		total += products[k].count * products[k].price;
	}
	localStorage.setItem("total", total);
	$(obj).closest('table').find('.table-total').text(total + ' руб.');
	cartUpdateTotal(obj);
	
}

function cartRemoveProduct(id, obj) {
	var products = localStorage.getItem('products');
	products = JSON.parse(products);
	delete products[id];
	localStorage.setItem("products", JSON.stringify(products));
	var total = 0;
	for (k in products) {
		if (products[k] == null) {
			continue;
		}
		total += products[k].count * products[k].price;
	}
	localStorage.setItem("total", total);
	cartUpdateTotal(obj);
}

function cartAddProduct(obj) {
	
	var type = $(obj).closest('.product').find('input[name=type]').val();
	var id = $(obj).closest('.product').find('input[name=id]').val();
	var additionalType = $(obj).closest('.product').find('input[name=additional-type]').val();
	
	var info = '';
	var price = parseInt($(obj).closest('.product').find('.price').text().replace(/\s+/g, ''));
	var productcount = parseInt($(obj).closest('.product').find('.field-count').val());
	if (type == 'bouquets') {
		var count_cupcake = parseInt($(obj).closest('.product').find('.field-cupcakes').val());
		var price_cupcake = parseInt($(obj).closest('.product').find('.price-cupcake').text().replace(/\s+/g, '')); 
		info = 'В букете ' + count_cupcake + ' капкейков';
		price = price + price_cupcake * count_cupcake;
	}

	if (type == 'cakes') {
		var count_cupcake = parseInt($(obj).closest('.product').find('.field-count-cupcake').val());
		var price_cupcake = parseInt($(obj).closest('.product').find('.price_cupcake').text().replace(/\s+/g, '')); 
		
		var weight = parseFloat($(obj).closest('.product').find('.field-weight').val()); 
		//console.log(weight + ' ВЫВОД');
		if (! (price > 0)) {
			price = 0;
		}
		if (additionalType != 'design-cakes' && weight != 1) {
			//price = price * weight;
			info += ' Вес - ' + weight + ' кг.';
		}
		
		if (count_cupcake > 0) {
			price = price + price_cupcake * count_cupcake;
			info += ' Капкейков - ' + count_cupcake + ' шт.';
			info += ' (тесто: ' + $(obj).closest('.product').find('.product-form .cupcake-dough').val() + ', начинка: ' + $(obj).closest('.product').find('.product-form .cupcake-stuffing').val() + ')';
		}
		
		var otherInfo = $(obj).closest('.product').find('input[name=other-info]').prop('checked');
		if (otherInfo) {
			var otherInfoPrice = parseInt($(obj).closest('.product').find('input[name=other-info]').parent().text().replace(/\D/g,''));
			price += otherInfoPrice;
			info += ' Добавить ягод.';
		}
		productcount = parseInt($(obj).closest('.product').find('.field-weight').val());
	}
	
	var product = {
		name : $(obj).closest('.product').find('.name').text(),
		article : $(obj).closest('.product').find('.article').text(),
		dough : $(obj).closest('.product').find('.product-form .dough').val(),
		stuffing : $(obj).closest('.product').find('.product-form .stuffing').val(),
		price : price,
		id : id,
		count : productcount,
		info: info,
		type: type,
		category: $('input[name=product_category]').val(),
		supplier_id: $('input[name=supplier_id]').val(),
		image : $(obj).closest('.product').find('.shk-image').attr('src')
	};
		
	var products = localStorage.getItem('products');
	
	if (products == undefined) {
		products = Array();
	} else {
		products = JSON.parse(products);
	}
	
	products.push(product);
	
	var total = 0;
	
	for (k in products) {
		if (products[k] == null) {
			continue;
		}
		total += products[k].count * products[k].price;
	}
		
	localStorage.setItem("total", total);

	
	localStorage.setItem("products", JSON.stringify(products));
				
}

function cartShow(obj)
{
	console.log('cartShow', localStorage.getItem('products'));
	
	var html = '<div class="heading">Содержимое заказа</div><table class="order-table">';

	var products = JSON.parse(localStorage.getItem('products'));
	var total = 0;
	
	var items = 0;	
	for (k in products) {
		if (products[k] == null) {
			continue;
		}
		if (products[k].type == 'cakes') unit = 'кг';
		else unit = 'шт.';
		html += '<tr><td class="col1">'+products[k].name+'<div style="color: #ccc; font-size: 11px;">' + products[k].article + '</div></td><td class="col2"><input type="number" min="1" value="'+products[k].count+'" /> '+unit+'<div style="color: #aaa; font-size: 10px;">' + products[k].price + ' руб.</div></td><td class="col3">'+ (products[k].price * products[k].count) +' руб.</td><td class="col4"><img src="/assets/img/delete.png" alt=""/><input type="hidden" value="'+k+'"></td></tr>';
		total += products[k].count * products[k].price;
		items++;
	}
	
	if (items == 0) {
			
		$(obj).closest('.product, .reason').find('form').fadeOut(500, function() {
			$(this).remove();
		});
		
	}
	
	html += '<tr class="last"><td class="col1 table-total" colspan="3" style="text-align: right;">' + total + ' руб.</td><td class="col4"></td></tr>';
	html += '</table>';
	
	localStorage.setItem("total", total);

	updateTopCart();

	$(obj).closest('.product').find('.form-data-product').html(html);
		
	cartUpdateTotal(obj);
	
}

function cartUpdateTotal(obj) {
	
	
	var total = parseInt(localStorage.getItem('total'));	
	var deliveryCheck = $(obj).closest('.product').find('input[name=delivery]').prop('checked');
	var deliveryPrice = parseInt($(obj).closest('.product').find('.delivery-price').text());
	var freeDelivery = parseInt($(obj).closest('.product').find('.free-delivery').text());
	
	if (freeDelivery < total) {
		deliveryPrice = 0;
		$(obj).closest('.product').find('.delivery-text').hide();
		$(obj).closest('.product').find('.delivery-text-more').show();
	} else {
		$(obj).closest('.product').find('.delivery-text-more').hide();
		$(obj).closest('.product').find('.delivery-text').show();
	}
		
	if (!deliveryCheck) {
		total += deliveryPrice;
	}
	
	$(obj).closest('.product').find('.order-price').text(total + ' ' + 'руб.');
}


$(document).ready(function() {
    
    $('#cart .cart-product, #info-reason').show();
	buildCatalog();
	coolResizeReviews();
	coolResize();
	coolResizeTrust();
	homeResize('cupcakes');
	homeResize('cakes');

	$('#design-order-button').on('click', function() {

		$('.product, .reason').find('form').remove();
		
		var html = $('#design-order').html();
		$(this).closest('.reason').append(html);
		$(this).closest('.reason').find('form').hide().fadeIn(500);
	});
	
	
	$('#reviews .review .button a').on('click', function(ev) {
		ev.preventDefault();

//		$('.product, .reason').find('form').remove();
		
		var html = $('#review-order').html();
		$(this).closest('.review').append(html);
		$(this).closest('.review').find('form').hide().fadeIn(500);
	});
	
	$(document).on('click', '.decision-continue', function() {
		
		if (parseInt($(this).closest('.product').find('.field-count').val()) < 1) {
			alert('Количество не может быть меньше 1');
			return;
		}
		
		cartAddProduct(this);
		updateTopCart();
		
		$(this).closest('.product').find('form').html('<div style="padding-top: 30px; font-size: 18px; font-weight: 300; font-family: Open Sans; text-align: center;">Товар добавлено в корзину!</div>').delay(500).fadeOut(500, function() {
			$(this).remove();
		});
	});

	$(document).on('click', '#cart .cart-heading, #cart .cart-info', function() {
		
		// check total
		
		var total = localStorage.getItem("total", total);
		if (total == 0) {
			alert('В корзине пусто. Добавьте что-то.');
			return;
		}
		
		var obj = cartFakeProduct(this);
		cartShow(obj);
		var product = $(obj).closest('.product');
		product.find('.field.decision').remove();
		product.find('.hidden-data').show();
		$('.field-phone').keypress(validateNumber);
	});

	
	$(document).on('click', '.product .info .article a', function(ev) {
		ev.preventDefault();
	});
	
	$(document).on('click', '.decision-order', function() {
		
		
		if (parseInt($(this).closest('.product').find('.field-count').val()) < 1) {
			alert('Количество не может быть меньше 1');
			return;
		}
		
		cartAddProduct(this);
		cartShow(this);
		var product = $(this).closest('.product');
		product.find('.field.decision').remove();
		product.find('.hidden-data').show();
	});
	function validateEmail(email) {
		var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
	function validateNumber(event) {
		var key = window.event ? event.keyCode : event.which;
	
		if (event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 37 || event.keyCode === 39) {
			return true;
		} else if ( key < 48 || key > 57 ) {
			return false;
		} else {
			return true;
		}
	}
    $('.field-phone').keypress(validateNumber);
	$(document).on('click', '.product-order-send', function() {
		var form = $(this).closest('.product');
		
		var deliveryCheck = $(form).find('input[name=delivery]').prop('checked');
		var deliveryPrice = parseInt($(form).find('.delivery-price').text());
		
		var data = {
			name: $(form).find('input[name=name]').val(),
			phone: $(form).find('input[name=phone]').val(),
			email: $(form).find('input[name=email]').val(),
			delivery_date: $(form).find('input[name=date]').val(),
			delivery_address: $(form).find('input[name=address]').val(),
			delivery_price: deliveryPrice,
			delivery_check: deliveryCheck,
			comment: $(form).find('input[name=comment]').val(),
			total: parseInt(localStorage.getItem('total')),
			products: localStorage.getItem('products')
		};
		var error = false
		if (data.name == '') {
            $(form).find('input[name=name]').css('border', '1px solid red');
            error = true;
			//alert('Введите ваше имя');
			//return;
		}else{
		  $(form).find('input[name=name]').css('border', 'none')
		}
		
		if (data.phone == '') {
            $(form).find('input[name=phone]').css('border', '1px solid red');
            error = true;
			//alert('Введите ваш телефон');
			//return;
		}else{
		  $(form).find('input[name=phone]').css('border', 'none')
		}
		
		if (data.email == '') {
		  $(form).find('input[name=email]').css('border', '1px solid red');
            error = true;
			//alert('Введите ваш email');
			//return;
		}else{
		  $(form).find('input[name=email]').css('border', 'none')
		}

		if (!validateEmail(data.email)) {
		  $(form).find('input[name=email]').css('border', '1px solid red');
            error = true;
			//alert('Введите ваш email');
			//return;
		}else{
		  $(form).find('input[name=email]').css('border', 'none')
		}

		

		if (data.delivery_date == '') {
		  $(form).find('input[name=date]').css('border', '1px solid red');
            error = true;
			//alert('Введите дату и время доставки');
			//return;
		}else{
		  $(form).find('input[name=date]').css('border', 'none')
		}
		data.pir_action = 'order/submit';
        if(error)
            return false;
		
		$(this).prop('disabled', true);
		
		$.ajax({
		  type: "POST",
		  url: '/ms2binder/',
		  //url: '/assets/php/order.php',
		  data: data,
		  beforeSend: function(){

            $('#loader_request').show();

          },

		  success: function(order_id) {
		  	localStorage.removeItem('products');
		  	localStorage.removeItem('total');
			$('#loader_request').hide();
			location.href = '/order-complete/?order_id=' + order_id;
			
		  }
		});
		
	});
	
	$(document).on('click', '#own-design-link', function(ev) {
		
		ev.preventDefault();
		
		
		var html = $('#design-order').html();
				
		/*$(this).parent().parent().find('.product').html(html);*/
		$(this).parent().parent().parent().parent().find('.product').html(html);
		/*$(this).parent().parent().find('.product').show();*/
		$(this).parent().parent().parent().parent().find('.product').show();
		
		var menuWidth = $('.sub-menu').width();
		if (menuWidth < 1257) {
			$(this).parent().parent().find('.product').css('left', 50);
			$(this).parent().parent().find('.product').css('right', 'auto');
		} else {
			$(this).parent().parent().find('.product').css('left', 'auto');
			$(this).parent().parent().find('.product').css('right', 50);
		}
		
		$(this).parent().parent().find('.product').find('form').hide().fadeIn(500);
		
		
	});
	
	$(document).on('click', '.design-order-send', function() {
		var form = $(this).closest('.product');
        
		var deliveryCheck = $(form).find('input[name=delivery]').prop('checked');
		var deliveryPrice = parseInt($(form).find('.delivery-price').text());
				
		var data = {
			name: $(form).find('input[name=name]').val(),
			phone: $(form).find('input[name=phone]').val(),
			count: $(form).find('input[name=count]').val(),
			email: $(form).find('input[name=email]').val(),
			individual: 1,
			delivery_price: deliveryPrice,
			delivery_check: deliveryCheck,
			delivery_date: $(form).find('input[name=date]').val(),
			delivery_address: $(form).find('input[name=address]').val(),
			comment: $(form).find('textarea[name=comment]').val(),
		};
		var error = false
		if (data.name == '') {
            $(form).find('input[name=name]').css('border', '1px solid red');
			//alert('Введите ваше имя');
			error = true;
		}else{
		  $(form).find('input[name=name]').css('border', 'none')
		}
		
		if (data.phone == '') {
            $(form).find('input[name=phone]').css('border', '1px solid red');
			//alert('Введите ваш телефон');
			error = true;
		}else{
		  $(form).find('input[name=phone]').css('border', 'none')
		}
		
		if (data.email == '') {
            $(form).find('input[name=email]').css('border', '1px solid red');
			//alert('Введите ваш email');
			error = true;
		}else{
		  $(form).find('input[name=email]').css('border', 'none')
		}
		
		if (data.comment == '') {
            $(form).find('textarea[name=comment]').css('border', '1px solid red');
			//alert('Заполните описание заказа');
			error = true;
		}else{
		  $(form).find('input[name=comment]').css('border', 'none')
		}
        if(error)
            return false;        
		
		var postData = new FormData();
        for(var i = 0, l = $(form).find('input[name=file]').prop('files').length; i < l; i++) {
            postData.append('file'+i, $(form).find('input[name=file]').prop('files')[i]);
        }
		postData.append('pir_action', 'order/submit');
		$.each(data, function(k, d) {
			postData.append(k, d);
		});
		
		$(this).prop('disabled', true);
		
		/*$.ajax({
		  type: "POST",
		  url: '/assets/php/order_design.php',
		  data: postData,
          dataType: 'JSON',
          xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
			beforeSend: function(){

            $('#loader_request').show();

          },
		  success: function(order_id) {
		      location.href = '/order-complete/?order_id=' + order_id;
			  $('#loader_request').hide();
		  },
		  contentType: false,
		  processData: false
		});*/
		
		
		
		
		
		$.ajax({
		  type: "POST",
		  url: '/ms2binder/',
		  //url: '/assets/php/order.php',
		  data: postData,
		  xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
		  beforeSend: function(){

            $('#loader_request').show();

          },

		  success: function(order_id) {
		  	
			$('#loader_request').hide();
			location.href = '/order-complete/?order_id=' + order_id;
			
		  },
		  contentType: false,
		  processData: false
		});
		
		
		
		
		
		
		
		
	});
	
	$(document).on('click', '.review-order-send', function() {

		var form = $(this).closest('.review');				
		var data = {
			name: $(form).find('input[name=name]').val(),
			email: $(form).find('input[name=email]').val(),
			proffesion: $(form).find('input[name=phone]').val(),
			url: $(form).find('input[name=url]').val(),
			comment: $(form).find('textarea[name=comment]').val(),
			file: $(form).find('input[name=file]').prop('files')[0]
		};
		
		if (data.name == '') {
			alert('Введите ваше имя');
			return;
		}
		
		if (data.comment == '') {
			alert('Введите текст отзыва');
			return;
		}
		
		var postData = new FormData();
		postData.append('file', $(form).find('input[name=file]').prop('files')[0]);
		$.each(data, function(k, d) {
			postData.append(k, d);
		});
		
		$(this).prop('disabled', true);
		
		$.ajax({
		  type: "POST",
		  url: '/assets/php/add-review.php',
		  data: postData,
		  beforeSend: function(){
			$('#loader_request').show();
          },
		  success: function(data) {
			location.href = '/review-sent/';
			$('#loader_request').hide();
		  },
		  contentType: false,
		  processData: false
		});
		
	});
	


	$(document).on('click', 'input[name=delivery]', function() {
		var total = parseInt(localStorage.getItem('total'));
		var obj = this;
		
		var deliveryCheck = $(obj).closest('.product').find('input[name=delivery]').prop('checked');
		var deliveryPrice = parseInt($(obj).closest('.product').find('.delivery-price').text());
	
		var freeDelivery = parseInt($(obj).closest('.product').find('.free-delivery').text());
	
		if (freeDelivery < total) {
			deliveryPrice = 0;
		}

		if (!deliveryCheck) {
			total += deliveryPrice;
		}
		
		$(obj).closest('.product').find('.order-price').text(total + ' ' + 'руб.');

	});

	

	
	$('.product .info .order, .price-order .order').on('click', function() {
		
		$('.product, .reason').find('form').remove();

		var hide_cake_data = 0;

		var min_count = $(this).closest('.product').find('input[name=min-count]').val();

		var type = $(this).closest('.product').find('input[name=type]').val();
		console.log(type);
		var name = $(this).closest('.product').find('.name').text();
		var article = $(this).closest('.product').find('.article').text();
		var dough = $(this).closest('.product').find('.dough').text().split(',');
		if($(this).closest('.product').find('.dough').text() == '') {
			dough = [];
		}
		var stuffing = $(this).closest('.product').find('.stuffing').text().split(',');
		if($(this).closest('.product').find('.stuffing').text() == '') {
			stuffing = [];
		}
		var price = $(this).closest('.product').find('.price').text();
		
		if (type == 'mini-cakes') {
			$('#product-order .label-stuffing').text('Вкус крема:');	
			$('#other-info').html('');
		} else if (type == 'cupcake') {
			$('#product-order .label-stuffing').text('Вкус начинки:');
			$('#other-info').html('');
		} else if (type == 'cakes') {
			//console.log(44444);
			$('#product-order .label-stuffing').text('Вкус крема:');
			var html2 = '';
			
			var price_cupcake = parseInt($(this).closest('.product').find('.price_cupcake').text()); 
			
			console.log('price_cupcake: ' + price_cupcake);
			if (price_cupcake > 0) {
				var price2 = parseInt(price);
				
				if (!(price2 > 0)) {
					hide_cake_data = 1;
				}
				
				html2 += '<br><div class="field">';
				html2 += '<label class="label1">Капкейков:</label> <input type="number" value="1" min="1" class="field-count-cupcake"> шт.';
				html2 += '</div>';
				console.log("ДОШЛИ ДО ЭТОЙ ТОЧКИ!");
				var common_dough = $(this).closest('.product').find('input[name=common_dough]').val().split(',');
				console.log(common_dough);
				var common_stuffing = $(this).closest('.product').find('input[name=common_stuffing]').val().split(',');


				html2 += '<div class="field">';
				html2 += '<label class="label1">Тесто капкейков:</label><select class="cupcake-dough"><option>не имеет значения</option>';
				
				$.each(common_dough, function (i, item) {
				    html2 += '<option>' + item + '</option>';
				});

				html2 += '</select></div>';

				html2 += '<div class="field">';
				html2 += '<label class="label1">Начинка капкейков:</label><select class="cupcake-stuffing"><option>не имеет значения</option>';
				
				$.each(common_stuffing, function (i, item) {
				    html2 += '<option>' + item + '</option>';				
				});
				html2 += '<option>ассорти</option></select></div>';
	
			}
				
			html2 += '<br><div class="field">';
			html2 += '<label class="label1">Вес:</label> <input type="text" class="field-weight" name="count" value="1"> кг';
			html2 += '</div>';
			html2 += '<div class="field"><input type="checkbox" name="other-info"> Добавить ягод (+ 500 руб. к стоимости)</div>';

				console.log(html2);
			$('#other-info').html(html2);
			$(this).closest('.product').find('.field-count').parent().find('.label-dough').text('Тесто торта:');
			
		} else if (type == 'bouquets') {
			$('#product-order .label-stuffing').text('Вкус крема:');
			
			var html2 = '<div class="field">';
			html2 += '<label class="label1">Капкейков в букете:</label> <select class="field-cupcakes"><option>3</option><option>5</option><option>7</option><option>9</option></select> шт.';
			html2 += '</div>';
			
			$('#other-info').html(html2);
		}

		$('#product-order .heading').text(name + ' 	(' + article + ')');
		$('#product-order .order-price').text(price);
		$('#product-order .dough').html('');
		$('#product-order .stuffing').html('');
	    $('#product-order .dough').append($('<option>', { 
	        value: 'не имеет значения',
	        text : 'не имеет значения'
	    }));

		 $('#product-order .stuffing').append($('<option>', { 
	        value: 'не имеет значения',
	        text : 'не имеет значения'
	    }));
	    
		$.each(dough, function (i, item) {
		    $('#product-order .dough').append($('<option>', { 
		        value: item,
		        text : item 
		    }));
		});
		
		$.each(stuffing, function (i, item) {
		    $('#product-order .stuffing').append($('<option>', { 
		        value: item,
		        text : item 
		    }));
		});
		
		if (type == 'cupcake' || type == 'bouquets') {
		    $('#product-order .stuffing').append($('<option>', { 
		        value: 'ассорти',
		        text : 'ассорти' 
		    }));
		}

		var html = $('#product-order').html();
		$(this).closest('.product').append(html);
		
		if (dough.length == 1 || dough.length == 0) {
			$(this).closest('.product').find('.label-dough').parent().hide();
		}

		if (stuffing.length == 1 || stuffing.length == 0) {
			$(this).closest('.product').find('.label-stuffing').parent().hide();
		}

		
		if (type == 'cakes') {
			$(this).closest('.product').find('.label-dough').text('Тесто торта:');	
			$(this).closest('.product').find('.label-stuffing').text('Начинка:');
			$(this).closest('.product').find('.field-weight').parent().find('label').text('Вес торта:');
            if($(this).closest('.product').find('input[name="min-count"]').val() !== '') {
				//console.log(88888);
                $(this).closest('.product').find('.field-weight').val(parseFloat($(this).closest('.product').find('input[name="min-count"]').val()));
                $(this).closest('.product').find('.field-weight').attr("min", parseFloat($(this).closest('.product').find('input[name="min-count"]').val()));
            } else {
				//console.log(99999);
                $(this).closest('.product').find('.field-weight').val(parseFloat($(this).closest('.product').find('.weight').text()));
                $(this).closest('.product').find('.field-weight').attr("min", parseFloat($(this).closest('.product').find('.weight').text()));
            }
			$(this).closest('.product').find('.field-count-cupcake').parent().find('label').text('Количество капкейков:');
			$(this).closest('.product').find('.label-stuffing').parent().after($(this).closest('.product').find('input[name=other-info]').parent());
			$(this).closest('.product').find('.label-stuffing').parent().after($(this).closest('.product').find('.field-weight').parent());
			$(this).closest('.product').find('.label-stuffing').parent().after($(this).closest('.product').find('.field-count').parent());
		}
		
		/*if (hide_cake_data) {
			$(this).closest('.product').find('.label-dough').parent().hide();
			$(this).closest('.product').find('.label-stuffing').parent().hide();
			$(this).closest('.product').find('input.field-weight').parent().hide();
			$(this).closest('.product').find('.form-data input[name=other-info]').parent().hide();
		}*/
		$('.field-phone').keypress(validateNumber);
		if (min_count != '' && min_count > 0) {
			$(this).closest('.product').find('.form-data .field-count').attr('value', min_count);
			$(this).closest('.product').find('.form-data .field-count').attr('min', min_count);
		}
		
		$(this).closest('.product').find('form').hide().fadeIn(500);
		
		
		// при изменении кол-ва капкейков при заказе
		$('.product .product-form .field-count-cupcake').on('change', function(e)
    	{
    	    console.log('change .field-count-cupcake ', e)
    	});
    	
	});
	
	
	
	
	
	
	$(document).on('click', '.form-close', function() {
		$(this).closest('.product, .reason, .review').find('form').fadeOut(500, function() {
			$(this).remove();
			$('.menu-reason').hide();
		});
	});
	
	$(document).on('change', '.order-table .col2 input', function() {
		var k = $(this).parent().parent().find('input[type=hidden]').val();
		cartUpdateProduct(k, this);
		//cartShow(this);
	});
	
	$(document).on('click', '.order-table .col4', function() {
		var k = $(this).find('input[type=hidden]').val();
		cartRemoveProduct(k, this);
		cartShow(this);
	});

	$('#feedback-button').on('click', function() {
	
		var name = $('#feedback-name').val();
		var phone = $('#feedback-phone').val();

		
		
		if (name == '') {
			alert('Укажите имя');
			return false;
		}
		if (phone == '') {
			alert('Укажите номер телефона');
			return false;
		}

		
	});
	
	updateTopCart();
	
	if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
		$('.product').css('cursor', 'pointer');
		$('.categories').css('cursor', 'pointer');
		$('.review').css('cursor', 'pointer');
		$('.reason').css('cursor', 'pointer');
	}
	
	$("#faq .aswer").css({'display': 'none'});
	
	$("#faq .question").click(function() {
		$(this).next(".aswer").slideToggle(500);
		$(this).toggleClass('open');
	});
	
	/*$('.block .question span').click(function() {
		
		$('.block .question span').not($(this)).removeClass('open');
		$('.question').not($(this).next('.question')).slideUp(100);
		$(this).next('.question').slideToggle(50);
		$(this).toggleClass('open');
		
	
	});*/

	if ($(window).width() > 659) {
	
		jQuery( document ).ready(function() {
			jQuery('#scrollup').mouseover( function(){
				jQuery( this ).animate({opacity: 1},0);
			}).mouseout( function(){
				jQuery( this ).animate({opacity: 0.7},0);
			}).click( function(){
				window.scroll(0 ,0); 
				return false;
			});

			jQuery(window).scroll(function(){
				if ( jQuery(document).scrollTop() > 0 ) {
					jQuery('#scrollup').fadeIn('fast');
				} else {
					jQuery('#scrollup').fadeOut('fast');
				}
			});
		});
	
	}
	
	if ($(window).width() < 659) {
	
		jQuery( document ).ready(function() {
			jQuery('#scrollup').mouseover( function(){
				jQuery( this ).animate({opacity: 1},0);
			}).mouseout( function(){
				jQuery( this ).animate({opacity: 1},0);
			}).click( function(){
				window.scroll(0 ,0); 
				return false;
			});

			jQuery(window).scroll(function(){
				if ( jQuery(document).scrollTop() > 0 ) {
					jQuery('#scrollup').fadeIn(0);
				} else {
					jQuery('#scrollup').fadeOut(0);
				}
			});
		});
	
	}
	
});

function updateTopCart() {
	
	
	var products = localStorage.getItem('products');
	
	products = JSON.parse(products);
	
	var total = 0;
	var count = 0;
	for (k in products) {
		if (products[k] == null) {
			continue;
		}
		total += products[k].count * products[k].price;
		count++;
	}
	
	countText = 'товаров';
	
	if (count == 1 || count == 21 || count == 31 || count == 43) {
		countText = 'товар';
	}
	if (count == 2 || count == 22 || count == 32 || count == 42) {
		countText = 'товара';
	}
	if (count == 3 || count == 23 || count == 33 || count == 43) {
		countText = 'товара';
	}
	if (count == 4 || count == 24 || count == 34 || count == 44) {
		countText = 'товара';
	}
		
	$('#cart .cart-info').text('В вашей корзине ' + count + ' ' + countText + ' на сумму ' + total + ' руб.');
	if (count == 0) {
		$('#cart .cart-info').text('В вашей корзине совсем пусто.');	
	}
}

function cartFakeProduct() {
	

	var obj = $('#cart .fakeButton');
	
	var html = $('#product-order').html();
	$(obj).closest('.product').append(html);
	$(obj).closest('.product').find('form').hide().fadeIn(500);

	return obj;	
}

$(window).resize(function() {
	coolResize();
	coolResizeReviews();
	homeResize('cupcakes');
	homeResize('cakes');
	coolResizeTrust();
});

$(document).on('click', '.trigger-order', function() {
    //$('.product-form').css('left', '500px')
    $('.product .order').trigger('click');
});


