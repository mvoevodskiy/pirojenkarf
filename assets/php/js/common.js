function declOfNum (number, titles) {
	cases = [2, 0, 1, 1, 1, 2];
	return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}

// var timerId;
function proverka(input, minVal, cartFlag) {
    var $input = $(input);
	var value = parseInt(+input.value);
	var myVal = parseInt(+minVal);
	
// 	clearTimeout(timerId);
// 	timerId = setTimeout(function() {
// 		if (value < myVal) {
// 			input.value = myVal;
// 			if (!cartFlag) {
// 				$('.alert_num').show();
// 			}
// 		}
// 		else {
// 			if (!cartFlag) {
// 				$('.alert_num').hide();
// 			}
// 		}
// 	}, 1000);
	
	// проверяем, число ли ввели
	if (isNaN(value)) {
	    input.value = myVal;
	    $input.trigger('change');
	}
	
	if (value < myVal) {
		input.value = myVal;
	    $input.trigger('change');
		if (!cartFlag) {
			$('.alert_num').show();
		}
	}
	else {
		if (!cartFlag) {
			$('.alert_num').hide();
		}
	}
}

function buildCatalog() {
	$('.reasons .reason').each(function() {
	    var selector = '.products .content #mse2_results > div';
		var n = $('.products .content #mse2_results > div').size();
		if (n < 2) {
    	    var selector = '.products .content > div';
    		var n = $(selector).size();
		}
		//if (n > 1) {
		    var randomN = 1 + Math.floor(Math.random() * n);
    		var className = $(this).attr('class');
    		var idx = className.replace('reason reason', '');
    		$('<div class="product reason reason' + idx + '">' + $(this).html() + '</div>').insertBefore(selector +':nth-child(' + randomN + ')');
    		$(this).remove();
		//}
	});
	$('.reasons').remove();
}

/*function buildCatalog() {
	$('.reasons .reason').each(function() {
		var n = $('.products .content > div').size();
		var randomN = 1 + Math.floor(Math.random() * n);
		var className = $(this).attr('class');
		var idx = className.replace('reason reason', '');
		$('<div class="product reason reason' + idx + '">' + $(this).html() + '</div>').insertBefore('.products .content > div:nth-child(' + randomN + ')');
		$(this).remove();
	});
	$('.reasons').remove();
}*/

function homeResize(type) {
	var catalogWidth = $('#' + type).width();
	var minProductWidth = 300;
	var maxProductWidth = 370;
	var realWidth;
	if (catalogWidth <= 600) {
		realWidth = 500;
		var items = 1;
		while (catalogWidth / items > minProductWidth) {
			items++;
		}
		items--;
		realWidth = catalogWidth / items;
		if (realWidth > 500) {
			realWidth = 500;
		}
	}
	else if (catalogWidth <= 1000) {
		realWidth = minProductWidth;
	}
	else {
		var items = 1;
		while (catalogWidth / items > minProductWidth) {
			items++;
		}
		items--;
		realWidth = catalogWidth / items;
		if (realWidth > maxProductWidth) {
			realWidth = maxProductWidth;
		}
	}
	$('#' + type + ' .categories').width(realWidth - 50);
	$('#' + type + ' .categories').height(realWidth - 50);
	$('#' + type + ' .categories').css('margin', 25);
}

function coolResize() {
	var catalogWidth = $('.products').width();
	var minProductHeight = 30;
	var minProductWidth = 300;
	var maxProductWidth = 450;
	var realWidth;
	if (catalogWidth <= 1000) {
		realWidth = minProductWidth;
	}
	else {
		var items = 1;
		while (catalogWidth / items > minProductWidth) {
			items++;
		}
		items--;
		realWidth = catalogWidth / items;
		if (realWidth > maxProductWidth) {
			realWidth = maxProductWidth;
		}
	}
	if (catalogWidth <= 600) {
		$('.product').width(realWidth);
		$('.product').height(realWidth);
		$('.product.reason').width(realWidth);
		$('.product.reason').height(realWidth);
	}
	else {
		$('.product').width(realWidth - 50);
		$('.product').height(realWidth - 50);
	}
	//$('.product').css('margin', 20);
	$('.cart-product').height('auto');
}

function coolResizeReviews() {
	var catalogWidth = $('#reviews').width();
	var minProductWidth = 300;
	var maxProductWidth = 370;
	var realWidth;
	if (catalogWidth <= 600) {
		realWidth = 500;
		var items = 1;
		while (catalogWidth / items > minProductWidth) {
			items++;
		}
		items--;
		realWidth = catalogWidth / items;
		if (realWidth > 500) {
			realWidth = 500;
		}
	}
	else if (catalogWidth <= 1000) {
		realWidth = minProductWidth;
	}
	else {
		var items = 1;
		while (catalogWidth / items > minProductWidth) {
			items++;
		}
		items--;
		realWidth = catalogWidth / items;
		if (realWidth > maxProductWidth) {
			realWidth = maxProductWidth;
		}
	}
	$('.review').width(realWidth - 50);
	$('.review').height(realWidth - 50);
	$('.review').css('margin', 25);
}

function coolResizeTrust() {
	var catalogWidth = $('#trust-us').width();
	var minProductWidth = 300;
	var maxProductWidth = 370;
	var realWidth;
	if (catalogWidth <= 600) {
		realWidth = 500;
		var items = 1;
		while (catalogWidth / items > minProductWidth) {
			items++;
		}
		items--;
		realWidth = catalogWidth / items;
		if (realWidth > 500) {
			realWidth = 500;
		}
	}
	else if (catalogWidth <= 1000) {
		realWidth = minProductWidth;
	}
	else {
		var items = 1;
		while (catalogWidth / items > minProductWidth) {
			items++;
		}
		items--;
		realWidth = catalogWidth / items;
		if (realWidth > maxProductWidth) {
			realWidth = maxProductWidth;
		}
	}
	if (catalogWidth <= 600) {
		$('#trust-us img').width(realWidth - 50);
		$('#trust-us img').height(realWidth - 50);
	}
	else {
		$('#trust-us img').width(realWidth - 50);
		$('#trust-us img').height(realWidth - 50);
	}
	$('#trust-us img').css('margin', 25);
}

// Устанавливает маску для телефона
function fieldPhoneMaskSet() {
    $('.phone_mask').inputmasks(window.maskOpts);
}

// Предварительный подсчёт цены
function calculateIntroPrice (product)
{
	var price = 0;

	product.price = +product.price.replace(/(^[^0-9]+)|([^0-9]+\+.+)|([^0-9]+)/g, '');

	if (product.type == 'cupcake' && !product.isSets)
	{
		var step_price = [50, 30, 20, 10],
			step_count = [6, 9, 13, 24],
		    totalCount = 0;

		var products = localStorage.getItem('products');
		if (products == undefined) {
			products = Array();
		}
		else {
			products = JSON.parse(products);
		}

		totalCount = totalCountProductsOfType(products, 'cupcake') + product.count;

		if (totalCount < step_count[0]) {
			price = product.price + step_price[0];
		}
		else if (totalCount < step_count[1]) {
			price = product.price + step_price[1];
		}
		else if (totalCount < step_count[2]) {
			price = product.price + step_price[2];
		}
		else if (totalCount < step_count[3]) {
			price = product.price + step_price[3];
		}
		else {
			price = product.price;
		}
	}
	else {
		price = product.price;
	}

	return "x "+ price +" = "+ (price * product.count) +" руб.";
}

// Мотивационные фразы
function motivationalPhrases (products, el, type, addCount)
{
	// console.log('motivationalPhrases el', el)
	// console.log('motivationalPhrases type', type)

	addCount = addCount || 0;

	var step_price = [20, 30, 40, 50],
		step_count = [6, 9, 13, 18, 21, 24],
		words = ['капкейк', 'капкейка', 'капкейков'],
		sum = 0,
		sum_next = 0,
		sum_last = step_price[step_price.length-1] * step_count[step_count.length-1];
		left_to = 0;

	var totalPrice = 0;
	var totalCount = 0;
	var text = '';

	if (type == 'cupcake')
	{
		totalCount = addCount + totalCountProductsOfType(products, type);
		// console.log('totalCount', totalCount)

		if (totalCount < step_count[0])
		{
			left_to = step_count[0] - totalCount;
			text = 'Отличный выбор! Добавьте еще '+ left_to +' '+ declOfNum(left_to, words) +' любого вида, чтобы уменьшить стоимость каждого капкейка на 20 рублей.';
		}
		else if (totalCount < step_count[1])
		{
			sum = step_price[0] * totalCount;
			left_to = step_count[1] - totalCount;
			text = 'Здорово! Вы сэкономили '+ sum +' рублей. Если вы хотите уменьшить стоимость каждого капкейка ещё на 10 рублей, добавьте еще '+ left_to +' '+ declOfNum(left_to, words) +' любого вида.';
		}
		else if (totalCount < step_count[2])
		{
			sum = step_price[1] * totalCount;
			left_to = step_count[2] - totalCount;
			text = 'Супер, вы уже сэкономили '+ sum +' рублей! Добавьте еще '+ left_to +' '+ declOfNum(left_to, ['любой', 'любых', 'любых']) +' '+ declOfNum(left_to, words) +', и вы уменьшите стоимость каждого еще на 10 рублей!';
		}
		else if (totalCount < step_count[3])
		{
			sum = step_price[2] * totalCount;
			left_to = step_count[5] - totalCount;
			text = 'Отлично! Вы уже сэкономили целых '+ sum +' рублей!';
		}
		else if (totalCount < step_count[4])
		{
			sum = step_price[2] * totalCount;
			left_to = step_count[5] - totalCount;
			text = 'Вы в двух шагах от максимальной скидки! Если положить в корзину ещё '+ left_to +' '+ declOfNum(left_to, words) +' на ваш выбор, то сегодня вы сэкономите '+ sum_last +' рублей!';
		}
		else if (totalCount < step_count[5])
		{
			sum = step_price[2] * totalCount;
			left_to = step_count[5] - totalCount;
			text = 'Вы в шаге от максимальной скидки! Если положить в корзину ещё '+ left_to +' '+ declOfNum(left_to, words) +' на ваш выбор, то сегодня вы сэкономите '+ sum_last +' рублей!';
		}
		else if (totalCount >= step_count[5])
		{
			sum = step_price[3] * totalCount;
			text = 'Поздравляем! Ваша скидка на капкейки составляет '+ sum +' рублей.';
		}

		/*if (totalCount >= 1 && totalCount <= 5)
		{
			if (totalCount == 3)
			{
				sum_next = step_price[0] * totalCount;
				text = 'Отличный выбор! Добавьте еще 3 капкейка любого вида, чтобы уменьшить стоимость каждого капкейка на 20 рублей.';
			}
		}
		else if (totalCount >= 6 && totalCount <= 8)
		{
			if (totalCount == 6)
			{
				sum = step_price[0] * totalCount;
				sum_next = step_price[1] * totalCount;
				text = 'Здорово! Вы сэкономили ' + sum + ' рублей. Если вы хотите уменьшить стоимость каждого капкейка ещё на 10 рублей, добавьте еще 3 капкейка любого вида.';
			}
		}
		else if (totalCount >= 9 && totalCount <= 12)
		{
			if (totalCount == 9)
			{
				sum = step_price[1] * totalCount;
				sum_next = step_price[2] * totalCount;
				text = 'Супер, вы уже сэкономили ' + sum + ' рублей! Добавьте еще 4 любых капкейка, и вы уменьшите стоимость каждого еще на 10 рублей!';
			}
		}
		else if (totalCount >= 13 && totalCount <= 23)
		{
			if (totalCount == 13)
			{
				sum = step_price[2] * totalCount;
				sum_next = step_price[3] * totalCount;
				text = 'Отлично! Вы уже сэкономили целых ' + sum + ' рублей!';
			}
			if (totalCount == 18)
			{
				sum = step_price[2] * totalCount;
				sum_next = step_price[3] * totalCount;
				text = 'Вы в двух шагах от максимальной скидки! Если положить в корзину ещё 6 капкейков на ваш выбор, то сегодня вы сэкономите 1200 рублей!';
			}
		}
		else if (totalCount >= 24)
		{
			sum = step_price[3] * totalCount;
			text = 'Поздравляем! Ваша скидка составляет ' + sum + ' рублей.';
		}*/
	}

	if (text != '') {
		el.show();
		el.html(text);
	}
	else {
		el.hide();
		el.html(text);
	}
}

// Считает общее кол-во товаров в корзине определённого типа
function totalCountProductsOfType (products, type)
{
	var totalCount = 0;

	for (k in products)
	{
		if (products[k] == null) { continue; }

		if (type == 'cupcake' && products[k].type == 'cupcake' && !products[k].isSets)
		{
			totalCount += +products[k].count;
		}
	}

	return totalCount;
}

// Высчитывает надценку на капкейки и подставляет новые цены в корзину
function discountOnGroupProducts (products, product)
{
	console.log('discountOnGroupProducts', products)

    var step_price = [50, 30, 20, 10],
		step_count = [6, 9, 13, 24],
	    totalPrice = 0,
	    totalCount = 0;

	totalCount = totalCountProductsOfType(products, 'cupcake');
	// console.log('totalCount', totalCount)

	for (k in products)
	{
		if (products[k] == null) { continue; }

		if (products[k].type == 'cupcake' && !products[k].isSets)
		{
			if (totalCount < step_count[0]) {
				products[k].upperprice = step_price[0];
			}
			else if (totalCount < step_count[1]) {
				products[k].upperprice = step_price[1];
			}
			else if (totalCount < step_count[2]) {
				products[k].upperprice = step_price[2];
			}
			else if (totalCount < step_count[3]) {
				products[k].upperprice = step_price[3];
			}
			else {
				products[k].upperprice = 0;
			}
		}

		$('.order-table tr[data-iterid="'+ k +'"] .cart-product-unit-price')
			.text((products[k].price + products[k].upperprice) + ' руб.');
		$('.order-table tr[data-iterid="'+ k +'"] .cart-product-count-price')
			.text((products[k].count * (products[k].price + products[k].upperprice)) + ' руб.');

		totalPrice += products[k].count * (products[k].price + products[k].upperprice);
	}

	$('.order-table .table-total').text(totalPrice + ' руб.');

    if (product.type == 'cupcake' && !product.isSets)
    {
	    motivationalPhrases(products, $('.order-table').closest('.form-data').find('.motivational-phrases').first(), product.type);
    }

	return products;
}

function cartUpdateProduct(id, obj) {
	var products = localStorage.getItem('products');
	products = JSON.parse(products);
	products[id].count = $(obj).val();
	if (products[id].type == 'cakes') {
		products[id].info = products[id].info.replace(/\d кг/, products[id].count + ' кг');
	}

	products = discountOnGroupProducts(products, products[id]);

	var total = 0;
	for (k in products) {
		if (products[k] == null) {
			continue;
		}
		total += products[k].count * (products[k].price + products[k].upperprice);
	}

	localStorage.setItem("total", total);
	$(obj).closest('table').find('.table-total').text(total + ' руб.');

	localStorage.setItem("products", JSON.stringify(products));
	$(obj).parent().parent().find('.cart-product-unit-price')
		.text((products[id].price + products[id].upperprice) + ' руб.');
	$(obj).parent().parent().find('.cart-product-count-price')
		.text((products[id].count * (products[id].price + products[id].upperprice)) + ' руб.');

	cartUpdateTotal(obj);
}

function cartRemoveProduct(id, obj) {
	var products = localStorage.getItem('products');
	products = JSON.parse(products);
	var clone_product = products[id];
	delete products[id];

	products = discountOnGroupProducts(products, clone_product);

	var total = 0;
	for (k in products) {
		if (products[k] == null) {
			continue;
		}
		total += products[k].count * (products[k].price + products[k].upperprice);
	}
	localStorage.setItem("total", total);
	localStorage.setItem("products", JSON.stringify(products));
	cartUpdateTotal(obj);
}

function cartAddProduct(obj) {
	var type = $(obj).closest('.product').find('input[name=type]').val();
	var isSets = +$(obj).closest('.product').find('input[name=isSets]').val();
	var id = $(obj).closest('.product').find('input[name=id]').val();
	var additionalType = $(obj).closest('.product').find('input[name=additional-type]').val();
	var info = '';
	var price = parseInt($(obj).closest('.product').find('.price').text().replace(/(^[^0-9]+)|([^0-9]+\+.+)|([^0-9]+)/g, '')); // было: /\s+/g
	var productcount = parseInt($(obj).closest('.product').find('.field-count').val());
	var productcountMin = parseInt($(obj).closest('.product').find('.field-count').attr('min'));
	if (type == 'bouquets') {
		var count_cupcake = parseInt($(obj).closest('.product').find('.field-cupcakes').val());
		var price_cupcake = parseInt($(obj).closest('.product').find('.price-cupcake').text().replace(/\s+/g, ''));
		info = 'В букете ' + count_cupcake + ' капкейков';
		price = price + price_cupcake * count_cupcake;
	}
	if (type == 'cakes') {
		var count_cupcake = parseInt($(obj).closest('.product').find('.field-count-cupcake').val());
		var price_cupcake = parseInt($(obj).closest('.product').find('.price_cupcake').text().replace(/\s+/g, ''));
		var weight = parseFloat($(obj).closest('.product').find('.field-weight').val());
		//console.log(weight + ' ВЫВОД');
		if (!(price > 0)) {
			price = 0;
		}
		if (additionalType != 'design-cakes' && weight != 1) {
			//price = price * weight;
			info += ' Вес - ' + weight + ' кг.';
		}
		if (count_cupcake > 0) {
			price = price + price_cupcake * count_cupcake;
			info += ' Капкейков - ' + count_cupcake + ' шт.';
			info += ' (тесто: ' + $(obj).closest('.product').find('.product-form .cupcake-dough').val() + ', начинка: ' + $(obj).closest('.product').find('.product-form .cupcake-stuffing').val() + ')';
		}
		var otherInfo = $(obj).closest('.product').find('input[name=other-info]').prop('checked');
		if (otherInfo) {
			var otherInfoPrice = parseInt($(obj).closest('.product').find('input[name=other-info]').parent().text().replace(/\D/g, ''));
			price += otherInfoPrice;
			info += ' Добавить ягод.';
		}
		productcount = parseInt($(obj).closest('.product').find('.field-weight').val());
	}
	var product = {
		name: $(obj).closest('.product').find('.name').text(),
		article: $(obj).closest('.product').find('.article').text(),
		dough: $(obj).closest('.product').find('.product-form .dough').val(),
		stuffing: $(obj).closest('.product').find('.product-form .stuffing').val(),
		price: price,
		upperprice: 0,
		id: id,
		count: productcount,
		counMin: productcountMin,
		info: info,
		type: type,
		isSets: isSets,
		category: $('input[name=product_category]').val(),
		supplier_id: $('input[name=supplier_id]').val(),
		image: $(obj).closest('.product').find('.shk-image').attr('src'),
	};
	var products = localStorage.getItem('products');
	if (products == undefined) {
		products = Array();
	}
	else {
		products = JSON.parse(products);
	}
	products.push(product);

	products = discountOnGroupProducts(products, product);

	var total = 0;
	for (k in products) {
		if (products[k] == null) {
			continue;
		}
		total += products[k].count * (products[k].price + products[k].upperprice);
	}

	localStorage.setItem("total", total);
	localStorage.setItem("products", JSON.stringify(products));
}

function cartShow(obj)
{
	var html = '<div class="heading">Содержимое заказа</div><table class="order-table">';
	var products = JSON.parse(localStorage.getItem('products'));
	var total = 0;
	var items = 0;
	var pkeys = [];

	for (k in products) {
		if (products[k] == null) {
			continue;
		}
		if (products[k].type == 'cakes') { unit = 'кг'; }
		else { unit = 'шт'; }
		html += '<tr data-iterid="' + k + '"><td class="col1">' + products[k].name + '<div style="color: #ccc; font-size: 11px;">' + products[k].article + '</div></td><td class="col2"><span class="btn-count-change btn-count-minus" data-operation="-">-</span><input type="text" data-type="number" min="' + products[k].counMin + '" value="' + products[k].count + '" onblur="return proverka(this,' + products[k].counMin + ',true);" data-id="' + products[k].id + '" /><span class="btn-count-change btn-count-plus" data-operation="+">+</span> ' + unit + '<div style="color: #aaa; font-size: 10px;"><span class="cart-product-unit-price">' + (products[k].price + products[k].upperprice) + ' руб.</span>'+ ((products[k].type == 'cupcake' && !products[k].isSets) ? '<span class="hints_'+ k +'" style="color:red;font-weight:bold;"><img src="/assets/img/question3.png" width="10" alt=""></span>' : '') +'</div></td><td class="col3"><div class="cart-product-count-price">' + ((products[k].price + products[k].upperprice) * products[k].count) + ' руб.</div></td><td class="col4"><img src="/assets/img/delete.png" alt=""/><input type="hidden" value="' + k + '"></td></tr>';
		total += products[k].count * (products[k].price + products[k].upperprice);
		items++;

		if (products[k].type == 'cupcake' && !products[k].isSets) {
		    pkeys.push(k);
		}
	}
	if (items == 0) {
		$(obj).closest('.product, .reason').find('form').fadeOut(500, function() {
			$(this).remove();
		});
	}
	html += '<tr class="last"><td class="col1 table-total" colspan="3" style="text-align: right;">' + total + ' руб.</td><td class="col4"></td></tr>';
	html += '</table>';
	localStorage.setItem("total", total);
	updateTopCart();
	$(obj).closest('.product').find('.form-data-product').html(html);
	cartUpdateTotal(obj);

    var cartOpentips = {};
    for (i in pkeys) {
        var hintHtml = $('#tplForHintCupcakes').html();

        $(hintHtml).find('.thisprice').each(function(index, el)
        {
            var $old = $(el).clone();

            $(el).html((products[pkeys[i]].price) + +$(el).html());

            hintHtml = hintHtml.replace($old.get(0).outerHTML, $(el).get(0).outerHTML);
        });

        cartOpentips[i] = new Opentip($('.hints_'+ pkeys[i]), { tipJoint: "bottom" });
        cartOpentips[i].setContent(hintHtml);
    }
    
    fieldPhoneMaskSet();
}

function cartUpdateTotal(obj) {
	var total = parseInt(localStorage.getItem('total'));
	var deliveryCheck = $(obj).closest('.product').find('input[name=delivery]').prop('checked');
	var deliveryPrice = parseInt($(obj).closest('.product').find('.delivery-price').text());
	var freeDelivery = parseInt($(obj).closest('.product').find('.free-delivery').text());
	if (freeDelivery < total) {
		deliveryPrice = 0;
		$(obj).closest('.product').find('.delivery-text').hide();
		$(obj).closest('.product').find('.delivery-text-more').show();
	}
	else {
		$(obj).closest('.product').find('.delivery-text-more').hide();
		$(obj).closest('.product').find('.delivery-text').show();
	}
	if (!deliveryCheck) {
		total += deliveryPrice;
	}
	$(obj).closest('.product').find('.order-price').text(total + ' ' + 'руб.');
}
$(document).ready(function()
{
    $('#cart .cart-product, #info-reason').show();
	buildCatalog();
	coolResizeReviews();
	coolResize();
	coolResizeTrust();
	homeResize('cupcakes');
	homeResize('cakes');

	$('#design-order-button').on('click', function() {
		$('.product, .reason').find('form').remove();
		var html = $('#design-order').html();
		$(this).closest('.reason').append(html);
		$(this).closest('.reason').find('form').hide().fadeIn(500);
		
		fieldPhoneMaskSet();
	});
	$('#reviews .review .button a').on('click', function(ev) {
		ev.preventDefault();
		//		$('.product, .reason').find('form').remove();
		var html = $('#review-order').html();
		$(this).closest('.review').append(html);
		$(this).closest('.review').find('form').hide().fadeIn(500);
	});
	$(document).on('click', '.decision-continue', function() {
		if (parseInt($(this).closest('.product').find('.field-count').val()) < 1) {
			alert('Количество не может быть меньше 1');
			return;
		}
		cartAddProduct(this);
		updateTopCart();
		$(this).closest('.product').find('form').html('<div style="padding-top: 30px; font-size: 18px; font-weight: 300; font-family: Open Sans; text-align: center;">Товар добавлено в корзину!</div>').delay(500).fadeOut(500, function() {
			$(this).remove();
		});
	});
	$(document).on('click', '#cart .cart-heading, #cart .cart-info', function() {
		// check total
		var total = localStorage.getItem("total", total);
		if (total == 0) {
			alert('В корзине пусто. Добавьте что-то.');
			return;
		}
		var obj = cartFakeProduct(this);
		cartShow(obj);
		var product = $(obj).closest('.product');
		product.find('.field.decision').remove();
		product.find('.hidden-data').show();
		$('.field-phone').keypress(validateNumber);
	});
	$(document).on('click', '.product .info .article a', function(ev) {
		ev.preventDefault();
	});
	$(document).on('click', '.decision-order', function() {
		if (parseInt($(this).closest('.product').find('.field-count').val()) < 1) {
			alert('Количество не может быть меньше 1');
			return;
		}
		cartAddProduct(this);
		cartShow(this);
		var product = $(this).closest('.product');
		product.find('.field.decision').remove();
		product.find('.hidden-data').show();
	});

	function validateEmail(email) {
		var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	function validateNumber(event) {
		var key = window.event ? event.keyCode : event.which;
		if (event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 37 || event.keyCode === 39) {
			return true;
		}
		else if (key < 48 || key > 57) {
			return false;
		}
		else {
			return true;
		}
	}
	$('.field-phone').keypress(validateNumber);
	$(document).on('click', '.product-order-send', function() {
		var form = $(this).closest('.product');
		var deliveryCheck = $(form).find('input[name=delivery]').prop('checked');
		var deliveryPrice = parseInt($(form).find('.delivery-price').text());
		var data = {
			name: $(form).find('input[name=name]').val(),
			phone: $(form).find('input[name=phone]').val(),
			email: $(form).find('input[name=email]').val(),
			delivery_date: $(form).find('input[name=date]').val(),
			delivery_address: $(form).find('input[name=address]').val(),
			delivery_price: deliveryPrice,
			delivery_check: deliveryCheck,
			comment: $(form).find('input[name=comment]').val(),
			total: parseInt(localStorage.getItem('total')),
			products: localStorage.getItem('products')
		};
		var error = false
		if (data.name == '') {
			$(form).find('input[name=name]').css('border', '1px solid red');
			error = true;
			//alert('Введите ваше имя');
			//return;
		}
		else {
			$(form).find('input[name=name]').css('border', 'none')
		}
		if (data.phone == '') {
			$(form).find('input[name=phone]').css('border', '1px solid red');
			error = true;
			//alert('Введите ваш телефон');
			//return;
		}
		else {
			$(form).find('input[name=phone]').css('border', 'none')
		}
		if (data.email == '') {
			$(form).find('input[name=email]').css('border', '1px solid red');
			error = true;
			//alert('Введите ваш email');
			//return;
		}
		else {
			$(form).find('input[name=email]').css('border', 'none')
		}
		if (!validateEmail(data.email)) {
			$(form).find('input[name=email]').css('border', '1px solid red');
			error = true;
			//alert('Введите ваш email');
			//return;
		}
		else {
			$(form).find('input[name=email]').css('border', 'none')
		}
		if (data.delivery_date == '') {
			$(form).find('input[name=date]').css('border', '1px solid red');
			error = true;
			//alert('Введите дату и время доставки');
			//return;
		}
		else {
			$(form).find('input[name=date]').css('border', 'none')
		}
		data.pir_action = 'order/submit';
		if (error) return false;
		$(this).prop('disabled', true);
		$.ajax({
			type: "POST",
			url: '/ms2binder/',
			//url: '/assets/php/order.php',
			data: data,
			beforeSend: function() {
				$('#loader_request').show();
			},
			success: function(order_id) {
				localStorage.removeItem('products');
				localStorage.removeItem('total');
				$('#loader_request').hide();
				location.href = '/order-complete/?order_id=' + order_id;
			}
		});
	});
	$(document).on('click', '#own-design-link, .own-design-link', function(ev) {
		ev.preventDefault();
		var html = $('#design-order').html();
		/*$(this).parent().parent().find('.product').html(html);*/
		$(this).parent().parent().parent().parent().find('.product').html(html);
		/*$(this).parent().parent().find('.product').show();*/
		$(this).parent().parent().parent().parent().find('.product').show();
		var menuWidth = $('.sub-menu').width();
		if (menuWidth < 1257) {
			$(this).parent().parent().find('.product').css('left', 50);
			$(this).parent().parent().find('.product').css('right', 'auto');
		}
		else {
			$(this).parent().parent().find('.product').css('left', 'auto');
			$(this).parent().parent().find('.product').css('right', 50);
		}
		$(this).parent().parent().find('.product').find('form').hide().fadeIn(500);
		
		fieldPhoneMaskSet();
	});
	$(document).on('click', '.design-order-send', function() {
		var form = $(this).closest('.product');
		var deliveryCheck = $(form).find('input[name=delivery]').prop('checked');
		var deliveryPrice = parseInt($(form).find('.delivery-price').text());
		var data = {
			name: $(form).find('input[name=name]').val(),
			phone: $(form).find('input[name=phone]').val(),
			count: $(form).find('input[name=count]').val(),
			email: $(form).find('input[name=email]').val(),
			individual: 1,
			delivery_price: deliveryPrice,
			delivery_check: deliveryCheck,
			delivery_date: $(form).find('input[name=date]').val(),
			delivery_address: $(form).find('input[name=address]').val(),
			comment: $(form).find('textarea[name=comment]').val(),
		};
		var error = false
		if (data.name == '') {
			$(form).find('input[name=name]').css('border', '1px solid red');
			//alert('Введите ваше имя');
			error = true;
		}
		else {
			$(form).find('input[name=name]').css('border', 'none')
		}
		if (data.phone == '') {
			$(form).find('input[name=phone]').css('border', '1px solid red');
			//alert('Введите ваш телефон');
			error = true;
		}
		else {
			$(form).find('input[name=phone]').css('border', 'none')
		}
		if (data.email == '') {
			$(form).find('input[name=email]').css('border', '1px solid red');
			//alert('Введите ваш email');
			error = true;
		}
		else {
			$(form).find('input[name=email]').css('border', 'none')
		}
		if (data.comment == '') {
			$(form).find('textarea[name=comment]').css('border', '1px solid red');
			//alert('Заполните описание заказа');
			error = true;
		}
		else {
			$(form).find('input[name=comment]').css('border', 'none')
		}
		if (error) return false;
		var postData = new FormData();
		for (var i = 0, l = $(form).find('input[name=file]').prop('files').length; i < l; i++) {
			postData.append('file' + i, $(form).find('input[name=file]').prop('files')[i]);
		}
		postData.append('pir_action', 'order/submit');
		$.each(data, function(k, d) {
			postData.append(k, d);
		});
		$(this).prop('disabled', true);
		/*$.ajax({
		  type: "POST",
		  url: '/assets/php/order_design.php',
		  data: postData,
          dataType: 'JSON',
          xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
			beforeSend: function(){

            $('#loader_request').show();

          },
		  success: function(order_id) {
		      location.href = '/order-complete/?order_id=' + order_id;
			  $('#loader_request').hide();
		  },
		  contentType: false,
		  processData: false
		});*/
		$.ajax({
			type: "POST",
			url: '/ms2binder/',
			//url: '/assets/php/order.php',
			data: postData,
			xhr: function() {
				var myXhr = $.ajaxSettings.xhr();
				return myXhr;
			},
			beforeSend: function() {
				$('#loader_request').show();
			},
			success: function(order_id) {
				$('#loader_request').hide();
				location.href = '/order-complete/?order_id=' + order_id;
			},
			contentType: false,
			processData: false
		});
	});
	$(document).on('click', '.review-order-send', function() {
		var form = $(this).closest('.review');
		var data = {
			name: $(form).find('input[name=name]').val(),
			email: $(form).find('input[name=email]').val(),
			proffesion: $(form).find('input[name=phone]').val(),
			url: $(form).find('input[name=url]').val(),
			comment: $(form).find('textarea[name=comment]').val(),
			file: $(form).find('input[name=file]').prop('files')[0]
		};
		if (data.name == '') {
			alert('Введите ваше имя');
			return;
		}
		if (data.comment == '') {
			alert('Введите текст отзыва');
			return;
		}
		var postData = new FormData();
		postData.append('file', $(form).find('input[name=file]').prop('files')[0]);
		$.each(data, function(k, d) {
			postData.append(k, d);
		});
		$(this).prop('disabled', true);
		$.ajax({
			type: "POST",
			url: '/assets/php/add-review.php',
			data: postData,
			beforeSend: function() {
				$('#loader_request').show();
			},
			success: function(data) {
				location.href = '/review-sent/';
				$('#loader_request').hide();
			},
			contentType: false,
			processData: false
		});
	});
	$(document).on('click', 'input[name=delivery]', function() {
	    	    
		var total = parseInt(localStorage.getItem('total'));
		var obj = this;
		var deliveryCheck = $(obj).closest('.product').find('input[name=delivery]').prop('checked');
		var deliveryPrice = parseInt($(obj).closest('.product').find('.delivery-price').text());
		var freeDelivery = parseInt($(obj).closest('.product').find('.free-delivery').text());
		if (freeDelivery < total) {
			deliveryPrice = 0;
		}
		if (!deliveryCheck) {
			total += deliveryPrice;
		}
		$(obj).closest('.product').find('.order-price').text(total + ' ' + 'руб.');
	});
	
	$('.product .info .order, .price-order .order').on('click', function() {
	  	   //   alert("click");
		$('.product, .reason').find('form').remove();
		var $product = $(this).closest('.product');
		var hide_cake_data = 0;
		var id = +$product.find('input[name=id]').val();
		var min_count = $product.find('input[name=min-count]').val();
		var type = $product.find('input[name=type]').val();
		var isSets = +$product.find('input[name=isSets]').val();
		var name = $product.find('.name').text();
		var article = $product.find('.article').text();
		var dough = $product.find('.dough').text().split(',');
		if ($product.find('.dough').text() == '') {
			dough = [];
		}
		var stuffing = $product.find('.stuffing').text().split(',');
		if ($product.find('.stuffing').text() == '') {
			stuffing = [];
		}
		var price = $product.find('.price').text();
		
		var html = $('#product-order').html();
// 		console.log('.product-form')
		$product.append(html);
		
		if (type == 'mini-cakes') {
			$product.find('.product-form .label-stuffing').text('Вкус крема:');
			$product.find('.product-form #other-info').html('');
		}
		else if (type == 'cupcake') {
			$product.find('.product-form .label-stuffing').text('Вкус начинки:');
			$product.find('.product-form #other-info').html('');
		}
		else if (type == 'cakes') {
			$product.find('.product-form .label-stuffing').text('Вкус крема:');
			var html2 = '';
			var price_cupcake = parseInt($product.find('.price_cupcake').text());
			console.log('price_cupcake: ' + price_cupcake);
			if (price_cupcake > 0) {
				var price2 = parseInt(price);
				if (!(price2 > 0)) {
					hide_cake_data = 1;
				}
				html2 += '<br><div class="field">';
				html2 += '<label class="label1">Капкейков:</label> <span class="btn-count-change btn-count-minus" data-operation="-">-</span><input type="text" data-type="number" value="1" min="1" class="field-count-cupcake"><span class="btn-count-change btn-count-plus" data-operation="+">+</span> шт.';
				html2 += '</div>';
				console.log("ДОШЛИ ДО ЭТОЙ ТОЧКИ!");
				var common_dough = $product.find('input[name=common_dough]').val().split(',');
				console.log(common_dough);
				var common_stuffing = $product.find('input[name=common_stuffing]').val().split(',');
				html2 += '<div class="field">';
				html2 += '<label class="label1">Тесто капкейков:</label><select class="cupcake-dough"><option>не имеет значения</option>';
				$.each(common_dough, function(i, item) {
					html2 += '<option>' + item + '</option>';
				});
				html2 += '</select></div>';
				html2 += '<div class="field">';
				html2 += '<label class="label1">Начинка капкейков:</label><select class="cupcake-stuffing"><option>не имеет значения</option>';
				$.each(common_stuffing, function(i, item) {
					html2 += '<option>' + item + '</option>';
				});
				html2 += '<option>ассорти</option></select></div>';
			}
			html2 += '<br><div class="field">';
			html2 += '<label class="label1">Вес:</label> <span class="btn-count-change btn-count-minus" data-operation="-">-</span><input type="text" data-type="number" class="field-weight" name="count" value="1"><span class="btn-count-change btn-count-plus" data-operation="+">+</span> кг<div class="intro-price"></div>';
			html2 += '<div class="alert_num" style="display:none;">Минимальное количество для заказа - <span id="alert_num_detail">' + min_count + '</span></div>';
			html2 += '</div>';
			html2 += '<div class="field"><input type="checkbox" name="other-info"> Добавить ягод (+ 300 руб./кг.)</div>';
			console.log(html2);
			$product.find('#other-info').html(html2);
			$product.find('.field-count').parent().find('.label-dough').text('Тесто торта:');
		}
		else if (type == 'bouquets') {
			$product.find('.product-form .label-stuffing').text('Вкус крема:');
			var html2 = '<div class="field">';
			html2 += '<label class="label1">Капкейков в букете:</label> <select class="field-cupcakes"><option>3</option><option>5</option><option>7</option><option>9</option></select> шт.';
			html2 += '</div>';
			$product.find('.product-form #other-info').html(html2);
		}
		$product.find('.product-form .heading').text(name + ' 	(' + article + ')');
		$product.find('.product-form .order-price').text(price);
		$product.find('.product-form .dough').html('');
		$product.find('.product-form .stuffing').html('');
		$product.find('.product-form .dough').append($('<option>', {
			value: 'не имеет значения',
			text: 'не имеет значения'
		}));
		$product.find('.product-form .stuffing').append($('<option>', {
			value: 'не имеет значения',
			text: 'не имеет значения'
		}));
		$.each(dough, function(i, item) {
			$product.find('.product-form .dough').append($('<option>', {
				value: item,
				text: item
			}));
		});
		$.each(stuffing, function(i, item) {
			$product.find('.product-form .stuffing').append($('<option>', {
				value: item,
				text: item
			}));
		});
		if (type == 'cupcake' || type == 'bouquets') {
			$product.find('.product-form .stuffing').append($('<option>', {
				value: 'ассорти',
				text: 'ассорти'
			}));
		}
		if (dough.length == 1 || dough.length == 0) {
			$product.find('.label-dough').parent().hide();
		}
		if (stuffing.length == 1 || stuffing.length == 0) {
			$product.find('.label-stuffing').parent().hide();
		}
		if (type == 'cakes') {
			$product.find('.label-dough').text('Тесто торта:');
			$product.find('.label-stuffing').text('Начинка:');
			$product.find('.field-weight').parent().find('label').text('Вес торта:');
			if ($product.find('input[name="min-count"]').val() !== '') {
				$product.find('.field-weight').val(parseFloat($product.find('input[name="min-count"]').val()));
				$product.find('.field-weight').attr("min", parseFloat($product.find('input[name="min-count"]').val()));
			}
			else {
				$product.find('.field-weight').val(parseFloat($product.find('.weight').text()));
				$product.find('.field-weight').attr("min", parseFloat($product.find('.weight').text()));
			}
			$product.find('.field-count-cupcake').parent().find('label').text('Количество капкейков:');
			$product.find('.label-stuffing').parent().after($product.find('input[name=other-info]').parent());
			$product.find('.label-stuffing').parent().after($product.find('.field-weight').parent());
			$product.find('.label-stuffing').parent().after($product.find('.field-count').parent());
			
			// Удалим field-count, если есть field-weight
			if ($product.find('.field-weight').length) {
			    console.log(type)
			    $product.find('.field-count').parent().remove();
			}
		}
		$('.field-phone').keypress(validateNumber);
		if (min_count != '' && min_count > 0) {
			$product.find('.form-data .field-count').attr('value', min_count);
			$product.find('.form-data .field-count').attr('min', min_count);
			$product.find('.form-data .field-count').attr('onblur', 'return proverka(this,' + min_count + ');');
// 			$product.find('.form-data .field-count').attr('onkeyup', 'return proverka(this,' + min_count + ');');
			$product.find('.form-data .field-count').attr('onchange', 'return proverka(this,' + min_count + ');');
			$product.find('.form-data .field-weight').attr('value', min_count);
			$product.find('.form-data .field-weight').attr('min', min_count);
			$product.find('.form-data .field-weight').attr('onblur', 'return proverka(this,' + min_count + ');');
// 			$product.find('.form-data .field-weight').attr('onkeyup', 'return proverka(this,' + min_count + ');');
			$product.find('.form-data .field-weight').attr('onchange', 'return proverka(this,' + min_count + ');');
		}
		$product.find('form').hide().fadeIn(500);

		var count = +$product.find('.form-data input[name=count]').val();

		$product.find('.intro-price').html(
			calculateIntroPrice({
				'id': id,
				'type': type,
				'isSets': isSets,
				'price': price.replace(/(^[^0-9]+)|([^0-9]+\+.+)|([^0-9]+)/g, ''),
				'count': count,
			})
		);
		
		$('.field-name').val($("#user-fullname").val());
        $('.field-email').val($("#user-email").val());
        $('.field-address').val($("#user-address").val());

		// при изменении кол-ва капкейков при заказе
		$('.product .product-form .field-count-cupcake').on('change', function(e) {
			console.log('change .field-count-cupcake ', e)
		});
	});
	$(document).on('click', '.form-close', function() {
	   // alert("close");
		$(this).closest('.product, .reason, .review').find('form').find('input[data-type=number]').each(function()
		{
			var minVal = +$(this).attr('min');
			if (minVal > +$(this).val()) {
				$(this).val(minVal);
				$(this).trigger('change');
			}
		});
		$(this).closest('.product, .reason, .review').find('form').fadeOut(500, function() {
			$(this).remove();
			$('.menu-reason').hide();
		});
	});
	
	// Клик на кнопку "-" или "+" рядом с полем "кол-во"
	$(document).on('click', '.btn-count-change', function(e)
	{
	   // console.log(this)
	    $(this).disableSelection();
	    
	    var $input = $(this).parent().find('input[data-type=number]');
		var count = +$input.val();
		var op = $(this).data('operation');
		
		if (op == '-') {
		    if (count > 1) {
		        --count;
		    }
		} else {
		    ++count;
		}
		
		$input.val(count); //.change()
		$input.trigger('change');
		$input.trigger('blur');
		$input.trigger('keyup');
	});

	// Изменение кол-ва в корзине
	$(document).on('change keyup blur', '.order-table .col2 input', function(e)
	{
		var k = $(this).parent().parent().find('input[type=hidden]').val();
		cartUpdateProduct(k, this);
		//cartShow(this);
	});

	// Изменение кол-ва при заказе
	$(document).on('change keyup blur', '.product .product-form input[name=count]', function(e)
	{
		var type = $(this).closest('.product').find('input[name=type]').val();
		var isSets = +$(this).closest('.product').find('input[name=isSets]').val();
		var id = +$(this).closest('.product').find('input[name=id]').val();
		var price = $(this).closest('.product').find('.price').text();
		var count = +$(this).val();
		var minCount = parseInt(+$(this).attr('min'));
        
        // Проверяем, соответствует ли введённое значение в поле "Кол-во" минимально возможному
        // console.log(this)
        // console.log(minCount)
        // proverka(this, minCount);
        
        // Показываем предварительную цену на заказ
		$(this).closest('.product').find('.intro-price').html(
			calculateIntroPrice({
				'id': id,
				'type': type,
				'isSets': isSets,
				'price': price.replace(/(^[^0-9]+)|([^0-9]+\+.+)|([^0-9]+)/g, ''),
				'count': count,
			})
		);

		var products = localStorage.getItem('products');
		if (products == undefined) {
			products = Array();
		}
		else {
			products = JSON.parse(products);
		}

		if (type == 'cupcake' && !isSets)
		{
			var el = $(this).closest('.form-data').find('.motivational-phrases').first();

			motivationalPhrases(products, el, type, count);
		}
	});

	$(document).on('click', '.order-table .col4', function() {
		var k = $(this).find('input[type=hidden]').val();
		cartRemoveProduct(k, this);
		cartShow(this);
	});
	$('#feedback-button').on('click', function() {
		var name = $('#feedback-name').val();
		var phone = $('#feedback-phone').val();
		if (name == '') {
			alert('Укажите имя');
			return false;
		}
		if (phone == '') {
			alert('Укажите номер телефона');
			return false;
		}
	});
	updateTopCart();
	if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
		$('.product').css('cursor', 'pointer');
		$('.categories').css('cursor', 'pointer');
		$('.review').css('cursor', 'pointer');
		$('.reason').css('cursor', 'pointer');
	}
	$("#faq .aswer").css({
		'display': 'none'
	});
	$("#faq .question").click(function() {
		$(this).next(".aswer").slideToggle(500);
		$(this).toggleClass('open');
	});
	/*$('.block .question span').click(function() {

		$('.block .question span').not($(this)).removeClass('open');
		$('.question').not($(this).next('.question')).slideUp(100);
		$(this).next('.question').slideToggle(50);
		$(this).toggleClass('open');


	});*/
	if ($(window).width() > 659) {
		jQuery(document).ready(function() {
			jQuery('#scrollup').mouseover(function() {
				jQuery(this).animate({
					opacity: 1
				}, 0);
			}).mouseout(function() {
				jQuery(this).animate({
					opacity: 0.7
				}, 0);
			}).click(function() {
				window.scroll(0, 0);
				return false;
			});
			jQuery(window).scroll(function() {
				if (jQuery(document).scrollTop() > 0) {
					jQuery('#scrollup').fadeIn('fast');
				}
				else {
					jQuery('#scrollup').fadeOut('fast');
				}
			});
		});
	}
	if ($(window).width() < 659) {
		jQuery(document).ready(function() {
			jQuery('#scrollup').mouseover(function() {
				jQuery(this).animate({
					opacity: 1
				}, 0);
			}).mouseout(function() {
				jQuery(this).animate({
					opacity: 1
				}, 0);
			}).click(function() {
				window.scroll(0, 0);
				return false;
			});
			jQuery(window).scroll(function() {
				if (jQuery(document).scrollTop() > 0) {
					jQuery('#scrollup').fadeIn(0);
				}
				else {
					jQuery('#scrollup').fadeOut(0);
				}
			});
		});
	}
});

jQuery.fn.extend({
    disableSelection : function() {
        this.each(function() {
            this.onselectstart = function() { return false; };
            this.unselectable = "on";
            jQuery(this).css({
                 '-moz-user-select': 'none'
                ,'-o-user-select': 'none'
                ,'-khtml-user-select': 'none'
                ,'-webkit-user-select': 'none'
                ,'-ms-user-select': 'none'
                ,'user-select': 'none'
            });
            // Для Opera
            jQuery(this).bind('mousedown', function() {
            	return false;
            });
        });
    }
});

function updateTopCart() {
	var products = localStorage.getItem('products');
	products = JSON.parse(products);
	var total = 0;
	var count = 0;
	for (k in products) {
		if (products[k] == null) {
			continue;
		}
		total += products[k].count * (products[k].price + products[k].upperprice);
		count++;
	}
	countText = 'товаров';
	if (count == 1 || count == 21 || count == 31 || count == 43) {
		countText = 'товар';
	}
	if (count == 2 || count == 22 || count == 32 || count == 42) {
		countText = 'товара';
	}
	if (count == 3 || count == 23 || count == 33 || count == 43) {
		countText = 'товара';
	}
	if (count == 4 || count == 24 || count == 34 || count == 44) {
		countText = 'товара';
	}
	$('#cart .cart-info').text('В вашей корзине ' + count + ' ' + countText + ' на сумму ' + total + ' руб.');
	if (count == 0) {
		$('#cart .cart-info').text('В вашей корзине совсем пусто.');
	}
}

function cartFakeProduct() {
	var obj = $('#cart .fakeButton');
	var html = $('#product-order').html();
	$(obj).closest('.product').append(html);
	$(obj).closest('.product').find('form').hide().fadeIn(500);
	$('.field-name').val($("#user-fullname").val());
    $('.field-email').val($("#user-email").val());
    $('.field-address').val($("#user-address").val());
	return obj;
}
$(window).resize(function() {
	coolResize();
	coolResizeReviews();
	homeResize('cupcakes');
	homeResize('cakes');
	coolResizeTrust();
});

// Для открытия окна с заказом в полной карте товара
$(document).on('click', '.trigger-order', function() {
	//$('.product-form').css('left', '500px')
	$('.product .order').trigger('click');
});
//прототипчик для свитчера
$.prototype.NewSwitcher = function(target){
    $(this).click(function(event) {
       $(target).hasClass('hidden') ? $(target).removeClass('hidden'):$(target).addClass('hidden');
    });
};